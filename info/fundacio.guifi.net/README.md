web: https://fundacio.guifi.net

# Informació de l'operador de telecomunicacions majorista de la Fundació

- 2019-03-14: hi ha contractat un ample de banda de 30Gbps i actualment es fan pics de 27.47 Gbps

- Sistema autònom (AS) de la fundació 49835
    - https://stat.ripe.net/AS49835#tabId=at-a-glance
    - http://as-rank.caida.org/asns/49835
    - https://www.peeringdb.com/asn/49835
    - https://bgpview.io/asn/49835
    - info extra http://es.wiki.guifi.net/wiki/Autonomous_System

# Informació sobre fibra òptica

- estesa de fibra 6€/m inclou TOT (mà d'obra, electrònica, etc.). Hi ha qui diu 10€/m i 100€/m si és obra civil / obrir carrer
- preu fix de 1500€ per llar (a vegades pot ser més alt si costa d'accedir a aquesta llar particularment), també hi ha un model mensual de 17€

# La importància de les donacions en l'estesa de fibra òptica de comuns

La millor forma que té la fundació guifi per garantitzar que la fibra òptica és i serà de comuns és a través dels apadrinaments en forma de donació que els usuaris (organitzacions, individus) fan a la fundació guifi per valor de 1500€ (hi han models de finançament i/o pagament en forma de quotes). Alguns operadors posen uns preus similars però ni informen a l'usuari ni a la fundació guifi de la operació, conseqüentment aquell tram de fibra òptica no és propietat de fundació guifi. Per tant es tracta d'un conflicte d'interès (telco amb ànim de lucre propietari d'un inmoble de comuns que pot privatitzar fàcilment).

Només una puntualització en relació a aquest paràgraf: Si bé és cert que si no es fan donacions la titularitat de la infraestructura és de l'operadora de telecomunicacions, també és cert que aquestes a través del PDSAiDR que presenten a ajuntaments, altres administracions i altres empreses co-ocupadores d'infraestructures abans de realitzar l'estesa fan una declaració responsable indicant que "Aquesta infraestructura, un cop desplegada, esdevindrà un bé comú que es posarà a disposició de tots els operadors comercials en les mateixes condicions neutrals, objectives, transparents, equitatives i no discriminatòries tal i com preveuen els Estatuts de la Fundació i el Comuns XOLN" i, d'altra banda, sempre constem com a promotors del Pla de Desplegament La Fundació. [Us n'adjunto un exemple](201801-PDSAiDR_Administracio.odt).

# Categories A, B, C

Es troba definit en l'apartat *C.3 Les categories* del document [d'activitat econòmica](201612-ConveniCompensacions_v14.odt)
