# FFDN ecosystem

## meeting 1/3 in Internet Freedom Festival 2019 according to ilico's perspective

ilico.org

- started in 2010
- 60-70 members
- 5-6 active members
- 100% volunteers / nonprofit
- 30 people uses vDSL (Orange) service, the supports them in general or another services like email, etc.

[FDN](https://www.fdn.fr/) (French Data Network) is the oldest ISP alive in France. Started in 1992 and is a nonprofit association. In 2009 there were 400 members, they wanted to do decentralization approach. So they helped people to copy what they were doing. In 2009-2010 7 organizations started to federate the FDN and they created a new non profit association called [FFDN](https://www.ffdn.org/) (Federation FDN), taking the good name and spreading it in the form of federation. Nowadays they are 30 nonprofit ISP associations.

On FFDN the FDN helps on administrative and legal stuff to become an ISP, also shares contracts they obtained (vDSL?). To obtain IPs and share datacenter resources you need to get them from another organizations.

ilico.org uses IPs from [gitoyen](https://www.gitoyen.net/) (founded in 2001 and located in Paris, mostly nonprofit association, gandi.net was a cofounder of this organization but they left it at some point with presence in Frankfurt and Amsterdam datacenters). They used also to share datacenter costs but they changed to [Grenode](https://www.grenode.net/) (founded in 2008 and located in Lyon and Grenoble, nonprofit association) as an strategic measure: more local, and more able to get interesting ISP contracts.

Ilico rented L2 links (using L2TP) from grenode place to reach gitoyen and use their own IPs from there.

Ilico meets with Grenode every 3 months and at least once a year for the FFDN general assembly.

Ilico expects to have FTTH at the end of 2019 because of the favorable conditions in their zone (https://fibre.ffdn.org/)

Both gatoyen and grenode uses [LibreNMS](https://www.librenms.org/) to manage datacenter stuff (LibreNMS is a community-based fork of the last GPL-licensed version of Observium)

## meeting 2/3 in Internet Freedom Festival 2019 according to ilico's perspective

[FFDN organizations in a map](https://db.ffdn.org/)

About FFDN governance:

- 1 organization 1 vote. But they used to reach consensus because voting system is very complicated to use
- copresidency between Benjamin Bayart and Oriane Piquer-Louis
    - Details: [Benjamin Bayart](https://fr.wikipedia.org/wiki/Benjamin_Bayart) was a [relevant president of FDN](https://fr.wikipedia.org/wiki/French_Data_Network#Pr%C3%A9sidence) and during his exercise started the decentralization of the FDN through a new meta-organization: the FFDN. In FFDN is holding the position of co-president, the same as Oriane Piquer-Louis. Some of the FFDN organizations would like to see a change in the organisation's culture so people from all of the member organizations can have better representation and participation.

A reference organization is [aquilenet.fr](https://www.aquilenet.fr/), 15% of its volunteers are in the board [la collégiale](https://www.aquilenet.fr/messagerie/)

The [FFDN wiki](https://www.ffdn.org/wiki/doku.php) contains information about all meetings they had. [Searching réunion](https://www.ffdn.org/wiki/doku.php?id=start&do=search&q=r%C3%A9union) got results.

They are developing an Information System developed in Django called [coin](https://code.ffdn.org/FFDN/coin)

Ilico does not use LDAP, but maybe Illyse organization (grenode)

## meeting 3/3 in Internet Freedom Festival 2019 according to ilico's perspective

[l2tpns](https://code.ffdn.org/l2tpns/l2tpns) is still heavily used (very stable). It was the thing being used by FDN, and a lot of FFDN organizations uses it. Other organizations use [mpd5](http://mpd.sourceforge.net/) as an alternative, or non of the solutions in case of pure wifi deployments. Freeradius is the AUTH part "just works". They used freeradius by config files and did not know about [daloradius](http://daloradius.com/)

They do not plan IPs in the guifi.net style (zones, subzones and related IP subnets). They try not to use the same ranges and in worst cases they would do BGP peerings

## TODO

- document here meeting with Baptiste and upload here the libreoffice calc
