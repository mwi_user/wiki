Information about the guifi LDAP (generated from users of guifi.net website)

This guide helps you to see if the ldap user works

Take in account that Drupal sync users with LDAP every day at 01:00

Install tooling for trying authentication via CLI

    apt-get install ldap-utils

Run (change myuser accordingly)

    ldapsearch -W -H ldaps://ldap.guifi.net -D "uid=myuser,o=webusers,dc=guifi,dc=net"

this is the output when ldap authentication succeeded:

```
# extended LDIF
#
# LDAPv3
# base <> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# search result
search: 2
result: 32 No such object

# numResponses: 1
```

this is the output when ldap authentication failed:

```
ldap_bind: Invalid credentials (49)
```
