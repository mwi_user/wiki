Research projects interesting for eXO and/or guifi and/or international community networks. This projects could be done by its community, external/conctracted professionals or academia deliverables

important notes:

- this document is a draft
- information here could be incorrect and/or outdated

<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
**Table of Contents**

- [Massive migration from bmx6 to bmx7](#massive-migration-from-bmx6-to-bmx7)
- [manage openwrt network with ansible](#manage-openwrt-network-with-ansible)
- [Decentralized DNS](#decentralized-dns)
- [(opensource?) MPLS research](#opensource-mpls-research)
- [radius, diameter and so on](#radius-diameter-and-so-on)
- [wireguard broker](#wireguard-broker)
- [Application to promote socialization between community network members](#application-to-promote-socialization-between-community-network-members)
- [Community network](#community-network)
  - [Network descriptor (proof of concept)](#network-descriptor-proof-of-concept)
  - [Template-based firmware](#template-based-firmware)
- [Online judge programming challenge resource](#online-judge-programming-challenge-resource)
- [Improve identity management in community networks](#improve-identity-management-in-community-networks)
- [Implementation of L2TP PPP forwarding for GNU/Linux](#implementation-of-l2tp-ppp-forwarding-for-gnulinux)
- [Network course tools](#network-course-tools)
- [Translation utility for plain git repositories](#translation-utility-for-plain-git-repositories)
- [Secure mesh](#secure-mesh)
- [Peerstreamer](#peerstreamer)
- [Retroshare](#retroshare)
- [Update GuifiProxy (proxy handler) plugin to Firefox Quantum](#update-guifiproxy-proxy-handler-plugin-to-firefox-quantum)
- [Improve wireguard](#improve-wireguard)
- [Solar node](#solar-node)
- [raspberry pi + spi interface](#raspberry-pi--spi-interface)
- [Open source implementation of TDMA for Linux kernel](#open-source-implementation-of-tdma-for-linux-kernel)
- [Open source implementation of 802.1aq for Linux kernel](#open-source-implementation-of-8021aq-for-linux-kernel)

<!-- END doctoc generated TOC please keep comment here to allow auto update -->

# Massive migration from bmx6 to bmx7

bmx6 is not compatible with bmx7, so this is as difficult as migrating from one routing protocol to another. As you are migrating nodes you are splitting the network. What would be the optimal migration? Could we take benefit from wireless link local?

The target network has 220 nodes (2018-10-8) you can [explore here](http://dsg.ac.upc.edu/qmpmon)

# manage openwrt network with ansible

find a way to have an orchestrator similar to ansible

- up to date? is it complete? implemented in ash? - https://github.com/gekmihesg/ansible-openwrt
- lua! outdated? - https://github.com/ronaldoafonso/ansible-lua-openwrt
- looks incomplete. outdated? - https://github.com/lefant/ansible-openwrt -> https://github.com/lefant?utf8=%E2%9C%93&tab=repositories&q=openwrt&type=&language=

- ansible docs - develop your own module
    - https://docs.ansible.com/ansible/latest/dev_guide/developing_modules.html
    - https://docs.ansible.com/ansible/2.3/dev_guide/developing_modules_general.html

# Decentralized DNS

Complexity: feasible for a developer

A (simple?) decentralized DNS solutions that fits in embedded devices (lightweight) to have FQDN services with letsencrypt certificates inside the community network. Interesting additional features: revision control/snapshots and trusting framework to improve control of the registers and avoid attacks.

Interesting reading: https://github.com/ansuz/dnssb/blob/master/docs/WHY.md

interesting features:

- based on trust (in a tree like IANA?) but easily forkable
- version control
- verifiable

Candidates:

- https://github.com/mwarning/KadNode
- https://github.com/torrentkino/torrentkino
- https://github.com/HarryR/ffff-dnsp2p

# (opensource?) MPLS research

references:

- https://docs.cumulusnetworks.com/display/DOCS/Segment+Routing
- https://www.youtube.com/watch?v=8LcniEuCuGM
- https://www.youtube.com/watch?v=jtCJaZTHK0E

# radius, diameter and so on

We used in a project http://daloradius.com/ as a WebGUI

https://en.wikipedia.org/wiki/RADIUS

https://en.wikipedia.org/wiki/Diameter_(protocol)

# wireguard broker

research on easy to use wireguard tunnels to allow certain compatible clients to easily improve security on their traffic (as an alternative of our l2tp unencrypted tunnel method)

check references:

- https://github.com/aparcar/wireguard-provisioning

# Application to promote socialization between community network members

Complexity: feasible for a developer

Inspired by [tubechat](http://tube.chat/) presentation in battlemesh, an application that improves the relation and interaction between the people involved in the community network would be an interesting application. Creative ways so they discover them on the same network they are; and ways to keep the contact.

# Community network

Research on the essential tools very important for a community network

## Network descriptor (proof of concept)

Complexity: feasible for a developer

This section assumes that autoconfiguration methodologies (libremesh, gluon) or a specific centralized technology (guifi.net's drupal) does not help to:

- get more people involved
- complexity equal to simple
- maintenance

The proposed approach is to get consensus on the full specification / description of a network. [Netjson](https://netjson.org) is a good try, but only describes specific nodes and its links. It is required the data of a zone as seen in guifi webpage

The proposition is to write a proof of concept tool in python where in a git repository (another research could include using other P2P/decentralized solutions like IPFS/IPLD, etc.):

- a zone is a directory
- `.zone` is a file that describes the zone: IP delegation to this node, who is admin or responsible, etc.
- a node is a file described in netjson format

note: when this description grows will go to a git repo itself

Inspiration:

- password-store.org
- netjson.org
- guifi webpage & cnml
- yanosz pointed me to:
    - python script to run VPNs https://github.com/freifunk/icvpn
    - data: https://github.com/freifunk/icvpn-meta
- Freifunk random address assignment: https://github.com/freifunk-bielefeld/firmware/tree/master/package/simple-radvd
  - Random addresses vs. Guifi planned network approach

Origin: guifipedro

## Template-based firmware

Complexity: feasible for a developer or integrator

Get one firmware for a specific device in a static oriented way (no autoconfiguration or the need of firmwares like libremesh or gluon)

- (2) Compilation through specific profile (device) and the packages required by its role => https://git.kbu.freifunk.net/yanosz/node-config-feed/
- (3) Template configuration: /etc/config => https://github.com/yanosz/mesh_testbed_generator/

quality measure: mark specific commit. fork relevant git repositories? routing?

Inspiration: Yanosz

Origin: guifilab / barcelona community

We started this => https://github.com/guifi-exo/temba/issues

# Online judge programming challenge resource

Educational and brain-fitness project to improve programming and developer skills for community network folks

# Improve identity management in community networks

The more services you have in the network, the more registers and logins users have to do. But well, this is a general problem that affects all organizations when use open source products and cannot spend time integrating the different services.

LDAP allows you to use always the same user and password. One user that have to login multiple times

SAML (and OpenID?) theoretically solves two problems better than LDAP:

- (optional) Single Sign On (SSO)
- Identity federation: "I don't know who are you, but let me point to the correct authority to validate you"

The project can revisit LDAP technology (a good WebGUI that is secure) or introduce SAML usage

Common LDAP service is `slapd` or openldap

I have never tried SAML software, but I found 4 open source implementation (all of them in java, brrr):

- https://www.shibboleth.net/
- this will be used by spanish universities http://adas-sso.com/en/
- https://www.keycloak.org/
- https://www.gluu.org/

# Implementation of L2TP PPP forwarding for GNU/Linux

Complexity: unknown

L2TPv3 ([RFC 3931](https://tools.ietf.org/html/rfc3931)) is a protocol that allows to encapsulate diferent layer 2 protocols in a layer 3 networks. The previous version of L2TPv3 defined in [RFC 2661](https://tools.ietf.org/html/rfc2661) only allows to encapsulate PPP frames. Actually the original L2TP is particulary useful in Guifi.net to establish Internet access in residential CPEs because it is very well supported by a great variety of OS and firmwares.
The actual implementations of L2TP for GNU/Linux support LNS and LAC basic features. In particular it is possible to stablish a L2TP tunnel from a LAC if the LAC itself starts the PPP connection. According to RFC 2661 is also possible to [forward PPP](https://tools.ietf.org/html/rfc2661#page-43) connection from remote PPP originators that is, PPP clients located in a diferent host than LAC (typically PPPoE clients).
Unfortunately none of the actual GNU/Linux implementations support this feature ([xltpd](https://www.xelerance.com/archives/155), [accel-ppp](https://accel-ppp.org/)).

The goal is to study one of the actual free softwares in order to add this new feature. Specially promising is accel-ppp since supports natively all the required protocols (PPPoE, L2TP), is well mantained, and is designed to implement a high performance L2TP access concentrators. Anyway, other options can be explored and analyzed.

- https://gitlab.com/guifi-exo/wiki/tree/master/howto/arquitectura-laa-acces-inet
- https://bsdrp.net/documentation/examples/pppoe_and_l2tp_lab?s%5B%5D=l2tp
- https://www.broadband-forum.org/technical/download/TR-025.pdf [section "8.2.2 L2TP Access Aggregation (LAA)"]
- https://lossociosblog.wordpress.com/author/rayh014/ [section "L2TP (Protocolo de túnel de capa dos)"]
- https://www.cisco.com/c/en/us/support/docs/dial-access/virtual-private-dialup-network-vpdn/23980-l2tp-23980.html [section "PPP and L2TP Flow Summary"]
- https://tools.ietf.org/html/rfc2661#section-4.4.5
- http://support.huawei.com/enterprise/docinforeader!loadDocument1.action?contentId=DOC1000010133&partNo=10062#vpn_l2tp_intro_05
- https://www.researchgate.net/figure/Figura-38-Establecimiento-de-una-conexion-L2TP_fig2_39697459
- https://gitlab.com/guifi-exo/wiki/tree/master/howto/l2tp-client-configuration
- https://accel-ppp.org/

# Network course tools

[do a network course](https://github.com/fgolemo/course-2017-networks) with [automatic correction](https://nodeschool.io/)

[extra tools that can be used to setup the tesbed](https://gitlab.com/guifi-exo/temba#testbed)

looks interesting:

- https://github.com/intrig-unicamp/mininet-wifi
- https://github.com/mininet/mininet/wiki/Simple-Router
- https://github.com/mininet/mininet/wiki/Teaching-and-Learning-with-Mininet

# Translation utility for plain git repositories

Explore translation tools that are compatible with git repositories with markdown files used as this wiki

# Secure mesh

note: I don't know if DFS stuff from secure mesh should be separated, probably later when we have more details. The thing is that they go together because they need wpasupplicant

Complexity: feasible for an integrator, good developer skills are very welcome (probably requires to read source code during the documentation phase)

Configuration and documentation about a network that is very secure and DFS ready

- Encrypted links
- bmx7 with its "trusted routing"

Origin: Daniel Gölle

extra: [DFS](../howto/dfs.md)

# Peerstreamer

Complexity: feasible for a developer and/or and integrator

- Clustering in the serverside
- How to put the streaming in RTP, how to play it?
- Get capture screen working, use cases:
    - get scream of presenter in a conference through web application (no specific cable: VGA, HDMI, etc.)
    - twitch.tv style service in a decentralized manner

Origin: netcommons.eu / http://peerstreamer.org/

# Retroshare

Complexity: feasible for an integrator, probably requires documentation and workshops

Discard nextcloud to upload massive content and other similar services. This puts risks on system administrators: it is better an encrypted P2P file sharing system. I see this as a deep and important step to avoid [self-censorship](https://en.wikipedia.org/wiki/Self-censorship) on our communities when we are on "sharing mode"

Origin: Gioacchino Mazzurco / http://retroshare.net/

# Update GuifiProxy (proxy handler) plugin to Firefox Quantum

This plugin is not only appreciated by guifi.net community but by some average Internet users that need to use several proxies

https://addons.mozilla.org/en-US/firefox/addon/guifiproxy/

extra https://guifi.net/node/32478

# Improve wireguard

Complexity: feasible for a developer but requires creative alternative way to solve the problem

Wireguard is not prepared yet for community networks. It uses a global clock and requires routers to synchronize NTP before they connect (chicken-egg problem)

Meanwhile, to mitigate the issue somehow, you can have an internal ntp server

# Solar node

Complexity: feasible for an integrator

OpenMPPT tracker, [presentation](https://github.com/elektra42/freifunk-open-mppt/blob/master/Freifunk-Open-MPPT-English.pdf)

18 V SOLAR PANEL 50 WATT [1](https://www.amazon.de/enjoysolar%C2%AE-Polykristallin-Solarmodul-Solarpanel-Wohnmobil/dp/B076B3Z48G?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duckduckgo-ffsb-de-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B076B3Z48G)

battery example [1](https://www.reichelt.com/de/en/Lead-Acid-Batteries-Kung-Long/WP-18-12/3/index.html?ACTION=3&LA=2&ARTICLE=44425&GROUPID=4232&artnr=WP+18-12&trstct=pol_14&SID=94WvXc46wQATMAAHIShQMfc78bb6a5f6916d42ae1066844b9e377) [2](https://www.amazon.de/LONG-Bleiakku-WP18-12I-12V-18Ah/dp/B0746J9F6Z?SubscriptionId=AKIAILSHYYTFIVPWUY6Q&tag=duckduckgo-ffhp-de-21&linkCode=xm2&camp=2025&creative=165953&creativeASIN=B0746J9F6Z)

Antena of 3W: tplink 841, tplink 510 (TODO: requires compatibility with latest version)

Translation needed from German to English -> https://elektrad.info/download/Freifunk-OpenMPPT-Handbuch-26-August-2017.pdf

Origin: Elektra

# raspberry pi + spi interface

Complexity: the skills needed are just find howtos and test them

Document the way to flash rom of bricked routers using SPI interface connector through the GPIO interface of raspberry pi and `flashrom` program

my notes:

```
reflashing eeprom of a device that is not booting

https://raspberrypi-aa.github.io/session3/spi.html
https://pinout.xyz/pinout/spi#

the command looks like this but can contain errors

  flashrom -p linux_spi:/dev/spidev0.0 spispeed=1000 -w /path/to/the/thing -V
```

[extra notes](../howto/spi.md)

# Open source implementation of TDMA for Linux kernel

Complexity: unknown

http://www.netshe.ru/tdma

Test [netshe products](http://www.netshe.ru/products) (accessed 2018-05-18)

```
NETSHe

    The OpenWRT derivative with compatible SDK and a set of unique software packages (like WebUI, initialization subsystem, wireless extensions) to create firmwares for wireless, networks and embedded devices. 

Modified wireless stack

    Modified wireless stack for Linux. Includes TDMA, Frequency, shifting and compression extensions. 

TDMA

    Implementation of time division multiply access for ordinary 802.11a/b/g/n hardware and Linux mac80211 wireless stack 
```

more info about [channel access](channel-access.md)

# Open source implementation of 802.1aq for Linux kernel

Complexity: unknown

https://en.wikipedia.org/wiki/IEEE_802.1aq

The "new" ethernet standard works mostly as MPLS (useful for multitenant network) but it is backward compatible with the old ethernet protocols which reduce its overall complexity. At the moment is not available in Linux platforms

