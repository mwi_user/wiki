Academic articles:

- [Network infrastructures the commons model for local participation, governance and sustainability. Leandro Navarro](https://www.apc.org/en/pubs/network-infrastructures-commons-model-local-participation-governance-and-sustainability). Ostrom distinguishes between (1) *resource system* to preserve and nurture and (2) *extractable resource* the thing that must be shared (and that can be sold as a service, for example connectivity). About the access, she talks about *bundle of rights*:
    - Access: The right to enter and connect to the network (contribute resources, link up).
    - Withdrawal: The right to *extract resources* from the system (obtain connectivity).
    - Management: The right to regulate usage and make improvements.
    - Exclusion: The right to determine who will have access and how this right can be transferred.
    - Alienation: The right to sell a portion of the resource (e.g. by professional participants selling connectivity to their customers).
- Property-rights regimes and natural resources: A conceptual analysis. Edella Schlager and Elinor Ostrom
- How types of goods and property pights jointly affect collective action. Elinor Ostrom
- Private and common property rights. Elinor Ostrom
- [Ostrom's Law: Property rights in the commons. Lee Anne Fennell](https://www.thecommonsjournal.org/articles/10.18352/ijc.252/)



Articles on the web:

- [The Commons – A Historical Concept of Property Rights](http://wealthofthecommons.org/essay/commons-%E2%80%93-historical-concept-property-rights)
