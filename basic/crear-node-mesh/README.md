Ens ubiquem a una zona que hi hagi possibilitat d'afegir nodes mesh (anar al final del document)

Creem un node de guifi tal com està explicat en [la guia de crear un node de guifi](../crear-node-guifi/README.md)

Es recomana provar-ho en [la instància de test](https://test.guifi.net) per tal equivocar-nos amb tranquilitat.

En la web del node li donem a afegir dispositiu de tipus *Wifi*

![](add-device.png)

ens portarà a un formulari de creació a l'inici hi ha tres camps importants:

![](nick-status-contact.png)

- nick: el nom del trasto dins del node
- status: per assignar una ip es necessita almenys posar-ho en estat testing
- contact: email de contacte, que hauria de ser el mateix que el del node, per on voldríem rebre les notificacions de canvis

més endavant hi ha 4 camps importants:

![](radio-firmware-mac-meshradio.png)

- radio model: posar ubiquiti airmax rocket/nano/loco o similar (són els models que acostumem fer servir a mesh)
- firmware: qMp
- mac address: si no la sabem, emplenar amb 00:00:00:00:00:00 (ja que és un camp requerit)
- fer click a no radios per desplegar aquest camp

llavors fer click en *add new radio*

![](add-new-radio.png)

anem a baix de tot i fem save and continue, d'aquesta manera ens reservarà aquest rang de IPs pel nostre trasto mesh (si estigués disponible)

![](save-and-continue.png)

podem tornar a la zona de mesh ràdio i veurem que ara hi ha una ràdio definida, click en el desplegable

![](1address.png)

i veurem això:

![](1address-expanded.png)

quan tornem a la pantalla del node, veurem el nou dispositiu afegit, amb el rang assignat i quina IP hauria de tenir

![](device-added.png)

## veure si una zona pot tenir nodes mesh

Una zona pot tenir nodes mesh quan a la pestanya xarxes

![](networks_tab.png)

Hauria de sortir una assignació de xarxa per a mesh

![](mesh_network.png)
