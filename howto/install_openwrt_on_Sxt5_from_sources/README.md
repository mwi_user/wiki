# Howto install Openwrt with mesh modules or Qmp on SXT Lite 5 from sources

The [OpenWRT](https://openwrt.org) firmware was used in the Mikrotik SXT Lite 5 (RBSXT5nDr2) but the most modern versions do not support it, due to a change in the file system, now `ubifs`. The problem occurs because the nand chip Samsung K9F1G08U0E does not support subpaging. Downloading the firmware of [Lede](https://lede-project.org) or the modern Openwrt releases will make the device inoperative, which can be recovered only with `tftp`. It is a transient problem, because it has already been fixed in the most recent kernels, although they have not been (yet) incorporated into Openwrt. Deal with other problems like US version devices, factory locked for certain frequencies used in Europe, needs custom kernel too. We will compile a new firmware from Openwrt sources (stable, 18.06.1 version). 

### Image Compiling

#### Download Openwrt sources

Don't compile as root, use a normal user.
```
git clone https://github.com/Openwrt/Openwrt.git -b v18.06.1
cd Openwrt
```
####  Patch the Openwrt sources

This is specific to SXT Lite 5. Kernel 4.17 solved problems with subpage writing in K9F1G08U0E nand chip, but Openwrt trunk uses now a version of 4.14 branch of kernel, and Openwrt stable a 4.9.120 version. We can use a specific [patch for SXT](./nand-samsung-no-subpage-write.patch), placed in our case in `/tmp`:
```
patch -p1 < /tmp/nand-samsung-no-subpage-write.patch
```
#### Install the feeds

If you want to include [Qmp](https://qmp.cat) you have to add feeds to `feeds.conf.default` file. If it's from [Temba](https://gitlab.com/guifi-exo/temba), this is not necessary, the default feeds are used.

This is the list of feeds needed for Qmp:

```
src-git packages https://git.Openwrt.org/feed/packages.git
src-git luci https://git.Openwrt.org/project/luci.git
src-git routing https://git.Openwrt.org/feed/routing.git
src-git telephony https://git.Openwrt.org/feed/telephony.git
src-git qmp https://dev.qmp.cat/qmp.git
src-git libremap https://github.com/libremap/libremap-agent-Openwrt.git;master
src-git b6m https://dev.qmp.cat/b6m.git;Openwrt
src-git ncr https://dev.qmp.cat/ncr.git;master
src-git gwck git: //qmp.cat/gwck.git
src-git bmx6 git: //qmp.cat/bmx6.git
```
and install the feeds, in Temba too:
```
./scripts/feeds update -a
./scripts/feeds install -a
```
#### Make configuration file

We create an initial configuration file `.config` where we choose the characteristics of the firmware and the packages to install

The basic installation includes:

a) The definition of the firmware. For the SXT it is:
```
CONFIG_TARGET_ar71xx=y
CONFIG_TARGET_ar71xx_mikrotik=y
CONFIG_TARGET_ar71xx_mikrotik_DEVICE_nand-large=y
```
b) The packages to include

Temba establishes a basic list, which we will use here. You can choose others:

bmx6-json bmx6-sms bmx6-uci-config bmx6-table luci luci-ssl luci-app-bmx6 tcpdump-mini iperf3 netperf iwinfo netcat mtr ip

The ip package is currently in busybox and the config indicates it as CONFIG_BUSYBOX_DEFAULT_FEATURE_IP_ROUTE which is already by default.
The configuration lines of the Temba list are:
```
CONFIG_PACKAGE_bmx6-json=m
CONFIG_PACKAGE_bmx6-sms=m
CONFIG_PACKAGE_bmx6-uci-config=m
CONFIG_PACKAGE_bmx6-table=m
CONFIG_PACKAGE_luci=m
CONFIG_PACKAGE_luci-ssl=m
CONFIG_PACKAGE_luci-app-bmx6=m
CONFIG_PACKAGE_iperf3=m
CONFIG_PACKAGE_mtr=m
CONFIG_PACKAGE_netcat=m
CONFIG_PACKAGE_netperf=m
CONFIG_PACKAGE_tcpdump-mini=m
CONFIG_PACKAGE_iwinfo=m
```
Qmp need only a metapackage with a 'flavour' instead. If you prefer a qmp image select one of his config options:
```
CONFIG_PACKAGE_qmp-big-node=m
```
or
```
CONFIG_PACKAGE_qmp-tiny-node=m
```
If you want the packages compiled together with the kernel, the options that identify them are indicated =y otherwise are indicated =m. This is the default option.

c) Output images:
 
In addition to the default image for Mikrotik devices an initramfs image is necessary for booting, as we will see below. We must indicate it in the `.config` with the option
```
CONFIG_TARGET_ROOTFS_INITRAMFS=y
```
If you want to use the imagebuilder, select it in the `.config` file:
```
CONFIG_IB=y
CONFIG_IB_STANDALONE=y
```
d) Make regulation domains local

A USA version of Mikrotik SXT Lite is distributed in Europe with USA regulatory settings hardcoded in EEPROM. If you have bought this device, using local regulatory domains in kernel can fix it. Add to `.config`:
```
CONFIG_ATH_USER_REGD=y
```
A patch needs to be used too as we will see later.

If you want to add packages or modules not listed here, wait until dependencies are auto added in the next command, then edit the `.config` file another time. Maybe any of your packages or modules will have been activated. As example, `ppp` family of packages and modules will be called for `luci` install.

Save your `.config` file and generate packages dependencies
```
make defconfig
```
This modifies our `.config` file. As alternative you could use `make menuconfig` and select manually all config options.


#### Configure Kernel

The only change in kernel configuration could be updating Domain Regulations Database, if necessary. This is optional and rarely used. As regulatory domains is local now we can update database. Add this option in one of the kernel configs, typically in the device config (here `target/linux/ar71xx/mikrotik/config-default`):
```
CONFIG_CFG80211_INTERNAL_REGDB=y
```
A patch needs to be used too.


#### Patch kernel and packages

a) OPTIONAL. Ignore the regulatory domains defined in the EEPROM: Used in US-version devices. If necessary, option `CONFIG_ATH_USER_REGD` must be selected in the `.config`. We will use a [patch for local regulatory domains](./963-ath-usr-regd.patch), copied to the kernel patch directory (`target/linux/ar71xx/patches-4.9`). This patch can be used in others devices with chip Atheros.

b) OPTIONAL. Update local regulatory database: You need to follow the previous step and select the option `CONFIG_CFG80211_INTERNAL_REGDB` in any kernel configuration file. As example of use we can apply a [patch](./962-regulatory-domains-database.patch), copied to the `wireless-regdb` package directory (`package/firmware/wireless-regdb/patches`) This patch is only for development purposes and can not be used outside of testing environments.


#### Compile
```
make 
```
This will create packages, images and in our case the imagebuilder (`tar.xz` compressed) in the bin directory (`bin/targets/ar71xx/mikrotik`). We need al least the initramfs image, `openwrt-ar71xx-mikrotik-vmlinux-initramfs.elf`. 

A manually install in device and package load with `opkg` could be your choice, using the default image, `openwrt-ar71xx-mikrotik-nand-large-squashfs-sysupgrade.bin`; we will use the imagebuilder instead, and will build a new default image.

### Working with imagebuilder

Imagebuilder and automated configuration software as Temba facilitate installation, by constructing the final image with selected packages and configuration files already completed.

Unpack imagebuilder in another directory, ex. `/tmp`:
```
cp bin/targets/ar71xx/mikrotik/openwrt-imagebuilder-ar71xx-mikrotik.Linux-x86_64.tar.xz /tmp
cd /tmp
tar -xvf openwrt-imagebuilder-ar71xx-mikrotik.Linux-x86_64.tar.xz
cd openwrt-imagebuilder-ar71xx-mikrotik.Linux-x86_64
```
Invoke the imagebuilder with:
- the profile (the profiles list is provided by `make info`), ej. `nand-large`.
- the list of packages.
- the configuration file directory for the device, ej. `/tmp/files`.

In our case:
```
make V=s image PROFILE=nand-large PACKAGES="bmx6-json bmx6-sms bmx6-uci-config bmx6-table luci luci-ssl luci-app-bmx6 tcpdump-mini iperf3 netperf iwinfo netcat mtr -ppp -ppp-mod -pppoe -kmod-ppp -kmod-pppoe -kmod-pppox" FILE=/tmp/files
```
This creates a new default image in `bin/targets/ar71xx/mikrotik`, in our case `openwrt-ar71xx-mikrotik-nand-large-squashfs-sysupgrade.bin`.

SXT files are standard Openwrt configuration files for simple devices whith one ethernet adapter, like Nanostation Loco, without wan interface. Openwrt don't use SXT internal leds, so no gpio configuration is included in system. Configure network and wireless with your preferences and bmx6 for your mesh as usual. Device's `transmision power` maxim value is 27 dB, and radio path is `platform/ar934x_wmac`. 

### Write the image to device

To record the Openwrt firmware on Mikrotik devices for the first time, two images must be used: A first one starting in memory and the final image to be written on device running the initial image. 

The device bootloader will be activated if we turn on the device with the RESET button pressed for about 25 seconds. The loader contains a `dhcp client` to obtain an ip and a `tftp client` to receive the firmware file, and once the file is obtained, it will execute it in memory. We can use `dnsmasq` as root to start both a DHCP server and a TFTP server while serving the memory image:
```
device=eth5
directory=/tmp/Openwrt/bin/targets/ar71xx/mikrotik
sudo ip a a 192.168.1.2/24 dev $device
sudo dnsmasq --interface=$device --dhcp-range=192.168.1.10,192.168.1.100 --dhcp-boot=openwrt-ar71xx-mikrotik-vmlinux-initramfs.elf --enable-tftp --tftp-root=$directory -d -p0 -K --log-dhcp --bootp-dynamic
```
Change variables with your values, both `device` and images `directory`.

This will start the image in device memory, to which we can send the final image with `scp`. IP device openwrt default is `192.168.1.1`, with no root password, but if we used the imagebuilder we could have a custom network configuration. The first paramater is the full image path:

```
scp /tmp/Openwrt/bin/targets/ar71xx/mikrotik/openwrt-ar71xx-mikrotik-nand-large-squashfs-sysupgrade.bin root@192.168.1.1:/tmp
```

Once copied to device we must log in and run `sysupgrade`:
```
ssh root@192.168.1.1
sysupgrade -n /tmp/openwrt-ar71xx-mikrotik-nand-large-squashfs-sysupgrade.bin
```
The device reboot itself and you can login with `luci` or `ssh` as usual.

