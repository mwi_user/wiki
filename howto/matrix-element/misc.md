<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [todo / extra](#todo-extra)
  - [already irc networks](#already-irc-networks)
  - [irc bridge to freenode](#irc-bridge-to-freenode)
    - [previous steps](#previous-steps)
    - [do the bridge!](#do-the-bridge)
  - [spam](#spam)
    - [configure IRC room to avoid spam](#configure-irc-room-to-avoid-spam)
    - [utility to kill spam](#utility-to-kill-spam)
    - [future work](#future-work)
  - [save disk space: purge history](#save-disk-space-purge-history)
  - [send HTML formatted messages](#send-html-formatted-messages)
  - [telegram bridge](#telegram-bridge)
  - [translating a telegram channel to riot](#translating-a-telegram-channel-to-riot)
  - [captcha](#captcha)
  - [specification for script that migrates user from one server to another](#specification-for-script-that-migrates-user-from-one-server-to-another)
    - [script to blank account in server A](#script-to-blank-account-in-server-a)
  - [use our own integration server](#use-our-own-integration-server)
    - [particular interest: monitoring with prometheus and grafana](#particular-interest-monitoring-with-prometheus-and-grafana)
  - [bridges](#bridges)
  - [bots](#bots)
    - [bash script to send notification to bot](#bash-script-to-send-notification-to-bot)
  - [do your own piwik analitics on matrix](#do-your-own-piwik-analitics-on-matrix)
- [matrix email bridge idea](#matrix-email-bridge-idea)
- [whatsapp and telegram bridge (take 2) idea](#whatsapp-and-telegram-bridge-take-2-idea)
- [dark things about matrix and riot](#dark-things-about-matrix-and-riot)
  - [extra notes 2019-2-22](#extra-notes-2019-2-22)
- [users dilemma](#users-dilemma)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->
# todo / extra

## already irc networks

https://github.com/matrix-org/matrix-appservice-irc/wiki/Bridged-IRC-networks

https://github.com/matrix-org/matrix-appservice-irc/issues/208

## irc bridge to freenode

This describes how to use official matrix bridge. The matrix discussion room is #irc:matrix.org

note: I hope for other IRC networks should be very similar

### previous steps

To do an IRC bridge you have to be operator (op)

Check the general state of the chat room with the following command

    /msg ChanServ info #channel

If you are alone in the room and seems that is not configured you can register it

    /msg ChanServ register #channel

If you loose the operator of your channel (you logged out), ask it to ChanServ

    /msg ChanServ op #channel yourUser

To register a room you require to register your current nickname

    /msg NickServ REGISTER password youremail@example.com

Change nickname

    /nick newUsername

To identify your existing user

    /msg NickServ IDENTIFY yourUser password

note: if the room is not configured but the room is full of people, nobody can claim to be operator. A solution is try to convince everyone to quit the room (difficult), or get help from #frenode channel/freenode staff (not tested)

/msg ChanServ info #coopdevs

References:

- Nickname Registration -> src https://freenode.net/kb/answer/registration

### do the bridge!

From Riot client in a matrix channel select *Manage Integrations*, select IRC, select the network Freenode, select operator (you need your IRC user logged and being operator of that channel), link

After some seconds you will receive a message like

```
<MatrixBridge> @youruser:matrix.example.com has requested to bridge 'My room in Matrix' (https://matrix.to/#/#myroominmatrix:matrix.example.com) with #my-room-in-IRC on this IRC network.
               Respond with 'yes' or 'y' to allow, or simply ignore this message to disallow. You have 300 seconds from when this message was sent.
<yourUser> y
<MatrixBridge> Thanks for your response, bridge request authorised.
```

## spam

### configure IRC room to avoid spam

With the following configuration everyone can read the channel but only registered IRC users can write to it. With the exception of matrix users (who are not registered accounts in IRC!):

    /mode #channel +qe $~a *!*@gateway/shell/matrix.org/*

if you are fed up of lots of login/logout traffic from spammers, you can ban unidentified users from IRC with exception of matrix bridge:

    /mode #channel +be $~a *!*@gateway/shell/matrix.org/*

check all/configuration of a channel with:

    /mode #channel bqeI

References

- info about channel modes -> src https://freenode.net/kb/answer/channelmodes
- bans and exceptions to channel modes -> src https://freenode.net/kb/answer/extbans
- extra info about appservice-irc from matrix.org -> https://github.com/matrix-org/matrix-appservice-irc/wiki/End-user-FAQ

### utility to kill spam

https://gitlab.com/guifi-exo/matrix-killspam


### future work

- maybe you would need this https://github.com/t2bot/synapse-simple-antispam

## save disk space: purge history

the most relevant script (untested): https://git.fosscommunity.in/disroot/matrix_scripts/synapse_scripts/blob/master/purge_matrix_rooms.sh

Interesting discussions:

- https://github.com/matrix-org/synapse/issues/1730
- https://github.com/matrix-org/synapse/pull/911
- https://github.com/matrix-org/synapse/issues/1621
- https://github.com/matrix-org/synapse/pull/2540/files

Relevant documentation:

- https://github.com/matrix-org/synapse/tree/master/docs/admin_api
- https://github.com/matrix-org/synapse/blob/master/docs/admin_api/purge_history_api.rst

## send HTML formatted messages

You can send HTML formatted messages (for example for bot/alert notification) with `curl`, at the moment this is [officially not documented](https://github.com/matrix-org/matrix-doc/issues/917)

check html message example in [send_message.env.example](./send_message.env.example)

and use it in [send_message.sh](./send_message.sh)

## telegram bridge

https://github.com/SijmenSchoon/telematrix

https://t2bot.io/telegram/

https://medium.com/@mujeebcpy/bridging-of-riot-and-telegram-cccb16a955f1

Comment about telegram bridge: A matrix els usuaris de telegram es veuen com un usuari de matrix, però els missatges escrits a matrix a telegram els diu un bot que diu "tal persona a dit"

## translating a telegram channel to riot

Only some people can send messages, and lots of listeners

https://telegram.org/faq_channels#q-what-39s-a-channel

Solution? You can restrict users from sending messages to the room and only accept Moderator, Admin, etc. The rule has the name "To send messages, you must be a"

In matrix-riot, there is no way to hide members of a channel

## captcha

https://github.com/matrix-org/synapse/blob/master/docs/CAPTCHA_SETUP.rst

## specification for script that migrates user from one server to another

implementation: work in progress

ingredients:

- matrix API
- userA_A: user A in server matrix.a.com
- userA_B: user A in server matrix.b.com

objective: perform migration from userA_A to userA_B

procedure:

- userA_A massively invites userA_B to all its rooms
- userA_B massively accepts invitations of userA_A
- userA_A puts the same permissions it has on userA_B
- export riot encryption keys from userA_A to file
- import riot encryption keys from file to userA_B
- [optional: blank account in server A] userA_A leaves all rooms she is in

### script to blank account in server A

```python
#!/usr/bin/env python3

 # motivation: 
 # - there are problems to deactivate account and rooms in matrix
 #     - https://github.com/matrix-org/synapse/issues/2017
 #     - https://github.com/matrix-org/synapse/issues/1853
 # - this will be part of the migration script for users between one server and another

 # this script login to a specific matrix account and LEAVES ALL ROOMS

 # requirements
 # - apt install python-pip3
 # - pip3 install wheel matrix_client

 # based on https://github.com/matrix-org/matrix-python-sdk/blob/master/samples/ChangeDisplayName.py
 # useful resource https://github.com/matrix-org/matrix-python-sdk/tree/master/matrix_client

import sys

from matrix_client.client import MatrixClient
from matrix_client.api import MatrixRequestError
from requests.exceptions import MissingSchema

 # access data

host="https://matrix.example.com"
username="myuser"
password="mypassword"

 # login

client = MatrixClient(host)

try:
    client.login_with_password(username, password)
except MatrixRequestError as e:
    print(e)
    if e.code == 403:
        print("Bad username or password.")
        sys.exit(4)
    else:
        print("Check your server details are correct.")
        sys.exit(2)
except MissingSchema as e:
    print("Bad URL format.")
    print(e)
    sys.exit(3)

 # actions

roomlist = client.get_rooms()

print("leaving rooms:")
 # list() does a copy of dictionary to avoid error: "RuntimeError: dictionary changed size during iteration" -> src https://stackoverflow.com/questions/11941817/how-to-avoid-runtimeerror-dictionary-changed-size-during-iteration-error
for room in list(roomlist.values()):
    room.leave()
    print("  " + room.room_id + " left")
```

## use our own integration server

- https://dimension.t2bot.io
- https://github.com/turt2live/matrix-dimension

### particular interest: monitoring with prometheus and grafana

- add grafana widget https://github.com/turt2live/matrix-dimension/issues/86
- prometheus alertmanager bot https://github.com/turt2live/matrix-wishlist/issues/28

## bridges

https://github.com/turt2live/t2bot.io

https://t2bot.io

with tchncs.de: https://ibcomputing.com/bridging-matrix-telegram-rooms/

## bots

tomeshnet bot remindering meetings: https://github.com/tomeshnet/meshbot

bot to interact with gitlab stuff https://github.com/maubot/gitlab https://matrix.org/docs/projects/bot/maulabbot.html

### bash script to send notification to bot

[send_message.sh](./send_message.sh)

another login script https://gitlab.com/vurpo/e2ebot/blob/master/login.py - The script is written for a special old version of the Python SDK that has E2E support added, so it'll probably work if you remove the encryption=True and device_id parts from the script

(you have to test this) when you login with a new device an access_token is generated, that access_token can be used until that device exist or you logout

## do your own piwik analitics on matrix

something useful

# matrix email bridge idea

requires this particular [Federated Matrix Identity Server - mxisd](https://github.com/kamax-matrix/mxisd)

https://github.com/kamax-matrix/matrix-appservice-email - you can use this provider to test https://t2bot.io/email/

then you can probably talk with https://delta.chat/en/ users

# whatsapp and telegram bridge (take 2) idea

https://matrix.org/blog/2019/02/26/bridging-matrix-with-whatsapp-running-on-a-vm/#respond

https://github.com/tulir/mautrix-telegram - you can use this provider to test https://t2bot.io/telegram/

https://github.com/tulir/mautrix-whatsapp

# dark things about matrix and riot

Political concern: [Main funder is?was? AMDOC](https://matrix.org/blog/who-is-matrix-org/), search amodoc-mossad on google and you can discover something interesting. The funding situation is strongly changing (see relevant notes)

Tecnological concern: matrix generate ALOT ot metadata on the central server, even if the communication is encrypted, the metadata are not and most of the software analysis to generate network maps use just metadata, more you have more the map is accurate

Riot client is not free. Such a hype for a server-client non-free [0] software which tries to act as a Person In The Middle interconnecting any other network...

[0] https://directory.fsf.org/wiki/Talk:Riot-im

relevant notes:

- matrix foundation 1/2 https://matrix.org/blog/2018/10/29/introducing-the-matrix-org-foundation-part-1-of-2/
- towards open governance https://matrix.org/blog/2018/06/20/towards-open-governance-for-matrix-org/
- https://matrix.org/blog/2018/04/26/matrix-and-riot-confirmed-as-the-basis-for-frances-secure-instant-messenger-app/
- https://matrix.org/blog/2018/01/29/status-partners-up-with-new-vector-fueling-decentralised-comms-and-the-matrix-ecosystem/
- https://matrix.org/blog/2017/07/07/a-call-to-arms-supporting-matrix/

## extra notes 2019-2-22

Here follows a backup of the insights from [Maximus](https://gitlab.com/maxidorius) and [this is the link to the original conversation](https://matrix.to/#/!RlmAHyIuKhyVybkriS:kamax.io/$1550849934243IAKmm:matrix.guifi.net?via=kamax.io&via=matrix.org&via=matrix.ordoevangelistarum.com)

Given the inability to implement and document a functional Matrix protocol by New Vector and resolve security vulnerabilities that are public for years, mxhsd will no longer try to implement the Matrix protocol. Instead it will implement what we (Kamax) think makes sense and document as we go. -> https://github.com/kamax-matrix/mxhsd

In no specific order:

- State resets which can be used to abuse permissions or create states that never existed (it was used last year to take over Matrix HQ per ex)
- The value of depth is not checked/validated properly, leading to possible room takeover attacks
- Until very recently, malicious/fake evens could be injected
- There are amplification/proxying attacks via the /send_join and /media endpoints

and that's just what's on top of my head - I didn't bother to keep scores in the end

there are also a whole set of (D)DoS attacks on a server by abusing server ACLs, max values of various fields and the likes

I've spent several years in the Matrix ecosystem and built a bunch of projects on it, and at least one for each spec (Client, Server, Application, Push). There is much more to it than what I just listed, but it would be a waste of time to discuss them further. I've already written things like a [Server ACL feature review](https://gist.github.com/maxidorius/b25769f1a89c8860b928babe795bbaaf) or just explained the various issues to length in various rooms and on my [18 months as a Matrix dev manifesto](https://gist.github.com/maxidorius/2f245351cf65534df730ccf262ebc6f4).

In terms of how to solve the issues, I know several ways to solve them personally, but they tend to be all related so it's faster to just take a different approach, which I am now doing using The Grid project (fork of Matrix) and this rewrite of mxhsd now called Gridepo.

I have tried my best to also report those issues and talk to the MAtrix dev about them before going down this road but to no avail. Matrix is deeply insecure and not privacy friendly at all. The only reason it has been getting away with it so far is because there is no alternative implementation that truely challenges the protocol and the implementations. If you look in the client world per example, many are just copying what Riot is doing instead of following the spec. In terms of servers, there hasn't been any successfully federated server ever until mxhsd & construct came along.

Given that we have to reverse engineer everything all the time, we just know were all the issues are, and we are now taking action to solve them the best way we can: forking.

As for promoting Riot, my personal opinion is that it just promotes a closed protocol/ecosystem as Riot is actually not Matrix compatible and very bad in terms of privacy. One of the worse horror story is that everytime you switch to a room, a HTTP call is made to vector.im with a token directly used to retrieve your Matrix ID. They can know exactly when you do something, your Matrix ID, etc. This can also be hijacked by a third-party potentially as there is no safeguard in place to prompt the user about sharing their personal info (All this done via the /openid endpoints).

This is the kind of thing that shows me there is no path forward in Matrix as the ecosystem is simply dominated by Riot and Synapse which do not follow the spec themselves and block any implementation that stick to the spec to actually be able to do anything. So you are force to implement the custom ways of Riot and synapse (whatever you build) which prevents from solving the security issues even if you wanted to.

3pid problem https://github.com/matrix-org/synapse/issues/4540

In the case of federation/state resolution/internal room stuff, I know that there is no homeserver to this day, synapse included, that actually implement the [S2S API]() spec. Even just trying to [send data](https://matrix.org/docs/spec/server_server/r0.1.1.html#put-matrix-federation-v1-send-txnid) fails with a 404 with synapse, because it's actually using another URL. (this is just the tip of the iceberg). So rather than spend more time reverse-engineering a moving target, I prefer invest my own time somewhere else.

## Users dilemma

A known problem is that until today (late april 2020) it is not possible to completely remove a user from the postgresql database, only unable them (and changing their names in the database is a bad idea, things will start to get very messy, being related to dirrefent tables).
That means that the username will not be able for new users.
If you use LDAP you want to keep the possibility to create new ldap users, without their names (uid= in ldap) to be already used in the matrix server. Which leads to close public registration on the server.
To be able to allow public registration, and still not have a complete caos when creating important LDAP users, as a workaround, after few trials and errors (and database dumps) I was able to overtake an existing matrix user with a new LDAP user: a very authoritarian way to do it!
  - when adding a new ldap user, the ldap username/password will allow login to matrix server.
  - once logged in the matrix server, change the password in the matrix settings (LDAP and matrix's postgresql passwords are different and both are valid to login the matrix server)
  - in matrix settings, write down the existing email address and delete it
  - somehow, you can add a new email address and verify it, but it will not be permanetly stored in the matrix server (LDAP is unaffected)
  - 