<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [icecast](#icecast)
- [libretime](#libretime)
  - [docs](#docs)
- [liquidsoap](#liquidsoap)
  - [install](#install)
    - [method 1 - latest version](#method-1-latest-version)
    - [method 2 - DYI](#method-2-dyi)
  - [daemonize](#daemonize)
  - [sending signals](#sending-signals)
  - [testing switch with (catalan) voice and sounds](#testing-switch-with-catalan-voice-and-sounds)
  - [alternative to libretime/airtime](#alternative-to-libretimeairtime)
  - [other references](#other-references)
- [other radios / examples](#other-radios-examples)
- [script to launch a liquidsoap icecast server to send your sound to another place](#script-to-launch-a-liquidsoap-icecast-server-to-send-your-sound-to-another-place)
- [send sound using udp experimental feature of liquidsoap](#send-sound-using-udp-experimental-feature-of-liquidsoap)
- [sender ffmpeg to receiver vlc](#sender-ffmpeg-to-receiver-vlc)
- [send the sound to another place (best realtime experience)](#send-the-sound-to-another-place-best-realtime-experience)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

## icecast

we assume that SSL/TLS/HTTPS is important (we are going to achieve: https://example.com:8443), that's why we are compiling it. if you don't care about doing `apt install icecast2` is fine

why we need to compile to get "security" capabilities:

- https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=744815
- https://trac.xiph.org/ticket/2310

```
apt-get build-dep icecast2
# trust me: https://downloads.xiph.org/releases -> https://ftp.osuosl.org/pub/xiph/releases/
wget https://ftp.osuosl.org/pub/xiph/releases/icecast/icecast-2.4.3.tar.gz
tar xvf icecast-2.4.3.tar.gz
cd icecast-2.4.3
./configure
make
make install
```

install icecast2 to get the service

    apt install icecast2

change /etc/init.d/icecast2

    #DAEMON=/usr/bin/icecast2
    DAEMON=/usr/local/bin/icecast

replace:

```
    <paths>
        <!-- basedir is only used if chroot is enabled -->
        <basedir>/usr/share/icecast2</basedir>

        <!-- Note that if <chroot> is turned on below, these paths must both
             be relative to the new root, not the original root -->
        <logdir>/var/log/icecast2</logdir>
        <webroot>/usr/share/icecast2/web</webroot>
        <adminroot>/usr/share/icecast2/admin</adminroot>
```

with:

```
    <paths>
        <!-- basedir is only used if chroot is enabled -->
        <basedir>/usr/local/share/icecast</basedir>

        <!-- Note that if <chroot> is turned on below, these paths must both
             be relative to the new root, not the original root -->
        <logdir>/var/log/icecast2</logdir>
        <webroot>/usr/local/share/icecast/web</webroot>
        <adminroot>/usr/local/share/icecast/admin</adminroot>
```

then we can remove icecast2 debian package

    apt remove icecast2

assuming you configured successfully a certbot / letsencrypt HTTPS certificate:

    cat /etc/letsencrypt/live/example.com/cert.pem /etc/letsencrypt/live/example.com/privkey.pem > /usr/local/share/icecast/icecast.pem

uncomment this:

    <listen-socket>
        <port>8443</port>
        <ssl>1</ssl>
    </listen-socket>

uncomment / adapt:

    <ssl-certificate>/usr/local/share/icecast/icecast.pem</ssl-certificate>

We cannot change or renew certificate reloading the service -> src https://wiki.xiph.org/Icecast_Server/known_https_restrictions

TODO smart cron to minimize number of restarts of the service. (!) A restart is going to break the streaming

references:

- check ssl in compilation src https://jksinton.com/articles/rpi-compiling-icecast-support-openssl
- service stuff inspired by src https://stackoverflow.com/questions/42188137/how-to-make-icecast-as-service-and-restart-it

## libretime

don't do this on a public server !!

```
git clone https://github.com/LibreTime/libretime
cd libretime
./install -p -i -a
```

### docs

switching between live and scheduled playout https://www.youtube.com/watch?v=qRqYtNsg-UM

## liquidsoap

### install

convince yourself early that you need to compile yourself liquidsoap (if you are on debian 9 stretch on 2018-03-26) - https://github.com/LibreTime/libretime/issues/192

is required to send signals and to have libretime working

to install icecast2 is enough to `apt install icecast2` and put appropriate passwords to avoid intrusion

#### method 1 - latest version

only first and last command as root, later we use a specific user

(tested with debian10)

```
# good general reference: https://www.liquidsoap.info/doc-1.4.1/install.html
# bug: because mccs is not dependency of opam
apt install opam mccs

adduser liquidsoap
gpasswd -a liquidsoap sudo

su -l liquidsoap

# bug: if we don't specify solver in a 4GB RAM PC does not work
# related info (partial help on the topic)
#   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=907636
#   https://bugs.debian.org/cgi-bin/bugreport.cgi?bug=908203
opam init --solver mccs -ya

# run next line if `ocaml --version` is lower than 4.08.0 (again, put solver mccs)
opam switch create 4.08.0 --solver mccs

# execute next line and put it in `~/.bashrc`
eval $(opam env)

# general
# next command will need sudo
opam depext taglib mad lame vorbis cry samplerate liquidsoap opus pulseaudio gstreamer
opam install taglib mad lame vorbis cry samplerate liquidsoap opus pulseaudio gstreamer
```

install "using opam" -> src http://liquidsoap.info/download.html

upgrade:

    opam update
    opam upgrade

#### method 2 - DYI

```
apt-get build-dep liquidsoap
apt-get source liquidsoap
cd liquidsoap-<version>
./configure
make
make install
```

src http://wiki.occupyboston.org/wiki/Multi-usb_mic/jackd/liquidsoap_setup

### daemonize

transform your liquidsoap script as a service

```
opam install liquidsoap-daemon
cd /to/the/directory/you/want/files/created # in my case: liked `cd ~`
daemonize-liquidsoap.sh
```

modify accordingly `~/script/main.liq`

after that:

    systemctl status main-liquidsoap.service

src https://github.com/savonet/liquidsoap-daemon

next service would be

    daemonize-liquidsoap.sh newservice

and modify accordingly `~/script/newservice.liq`

to remove:

    mode=remove init_type=systemd daemonize-liquidsoap newservice

### sending signals

send mp3 file (that loops indefinetely) or your input microphone (using pulseaudio)

explanation for jack -> src http://wiki.occupyboston.org/wiki/Multi-usb_mic/jackd/liquidsoap_setup

send directly to icecast

```liquidsoap
#!/usr/bin/liquidsoap
set("log.file.path", "/path/to/liq.log")
signal = single("/path/to/song.mp3")
#signal = input.pulseaudio(clock_safe=false)
output.icecast(%mp3,
     host = "<ip or URL>", port = 8000,
     password = "hackme", mount = "jazz.mp3",
     mksafe(signal))
```

send to libretime to master source

```liquidsoap
#!/usr/bin/liquidsoap
set("log.file.path", "/path/to/liq.log")
signal = single("/path/to/song.mp3")
#signal = input.pulseaudio(clock_safe=false)
output.icecast(%mp3,
     host = "<ip or URL>", port = 8001,
     password = "hackme", mount = "master",
     mksafe(signal))
```

### testing switch with (catalan) voice and sounds

extra requirements (voice synthesis)

    apt install festival festvox-ca-ona-hts

add to `/etc/festival.scm`:

    (set! voice_default voice_upc_ca_ona_hts)

strings with accents will be destroyed, for example "interrupció" -> http://festcat.talp.cat/en/usage.php

```
# concat strings -> https://bytesandbones.wordpress.com/2014/07/18/liquidsoap-concat-strings/
# random weigths -> src "A simple radio" http://savonet.sourceforge.net/doc-svn/quick_start.html
def template(text, freq)
  msg = random(
    weights = [1, 2, 10],
    [
      single("say: #{text}"),
      sine(480.0, amplitude = 0.1, duration = 0.5),
      blank(duration = 1.0)
    ])
  add([sine(freq, amplitude = 0.05), msg], normalize = true)
end

# A simple cross-fade -> src "Switch-based transitions" http://savonet.sourceforge.net/doc-svn/cookbook.html
def crossfade(a,b)
  add(normalize=false,
      [ sequence([ blank(duration=1.),
                   fade.initial(duration=2.,b) ]),
        fade.final(duration=1.,a) ])
end

init = template("tros de 0 a deu segons cada minut", 800.0)
init2 = template("tros de trenta a quaranta cinc segons cada minut", 1500.0)
#p1 = template("program number 1")
p2 = template("programa de deu a catorze hores", 440.0)
p3 = template("programa de quinze a vint hores", 440.0)
default_source = template("programa per defecte", 440.0)

default_transition = [
  crossfade, # init
  crossfade, # init2
  crossfade, # p2
  crossfade, # p3
  crossfade # default source
]

signal = switch(
  track_sensitive = false,
  transitions = default_transition,
  [
    ({ 0s-10s }, init),
    ({ 30s-45s }, init2),
    ({ 10h-14h }, p2),
    ({ 15h-20h }, p3),
    ({ true }, default_source)
  ])

#out(signal)

output.icecast(%mp3,
     host = "localhost", port = 8000,
     password = "hackme", mount = "test2.mp3",
     mksafe(signal))
```

### alternative to libretime/airtime

- main: take care of having always a fallback source (never stops, at the end we have silence running, and should never be modified or restarted). In this priority:
    - live1: intented to be for live with static icecast mountpoint
    - live2: intended to be for live with dynamic/spontaneous/ephemeral icecast mountpoint
    - schedule: executes the scheduled radio

```
su - liquidsoap
cd ~
daemonize-liquidsoap.sh main
daemonize-liquidsoap.sh live1
daemonize-liquidsoap.sh live2
daemonize-liquidsoap.sh schedule

# live1 and live2 should only be started manually
systemctl disable live1-liquidsoap.service
systemctl disable live2-liquidsoap.service
```

files of the service -> https://github.com/guifi-exo/xrcb-scheduler

### other references

https://www.linuxjournal.com/content/creating-internet-radio-station-icecast-and-liquidsoap

## other radios / examples

http://dir.xiph.org/

## script to launch a liquidsoap icecast server to send your sound to another place

tested in debian10 (debian9 is not going to work, then upgrade or use opam). Install liquidsoap:

    sudo apt install liquidsoap

on the server, create a file `script.liq` with:

```
set("log.file.path", "/tmp/script.liq.log")
# src "Usage" https://www.liquidsoap.info/doc-1.3.3/harbor.html
set("harbor.bind_addr","0.0.0.0")

# send sine as test
#source=sine(440.0, amplitude=0.01)
# send mixer output -> thanks https://sourceforge.net/p/savonet/mailman/message/28168771/
#   pacmd list-sources | grep "name:"
# (get the one with .monitor at the end)
mysoundcard="alsa_output.pci-0000_00_1b.0.analog-stereo.monitor"
source=input.pulseaudio(device=mysoundcard)
myport=8000

# thanks https://sourceforge.net/p/savonet/mailman/message/27916432/
output.harbor(%mp3,mount="test",port=myport,source)
# touching buffer, chunk, burst was a mess and people say "don't do it" https://sourceforge.net/p/savonet/mailman/message/23325265/
# +info: https://www.liquidsoap.info/doc-1.3.3/reference.html#output.harbor
```

on any client with web browser: http://<server_ip>:8000/test

## send sound using udp experimental feature of liquidsoap

on sender:

```
set("log.file.path", "/tmp/script.liq.log")

# src https://www.liquidsoap.info/doc-1.3.3/reference.html#output.udp

# send mixer output -> thanks https://sourceforge.net/p/savonet/mailman/message/28168771/
#   pacmd list-sources | grep "name:"
# (get the one with .monitor at the end)
mysoundcard="alsa_output.pci-0000_00_1b.0.analog-stereo.monitor"
source=input.pulseaudio(device=mysoundcard)

output.udp(%mp3(bitrate=128), host="192.168.16.80", port=5004, source)

# #not working, see below #1089
# output.opus(%opus(bitrate=128), host="192.168.16.80", port=5004, source)
```

on receiver:

```
set("log.file.path", "/tmp/script.liq.log")

# adjust buffer the lower you can. looks like mp3 is great with 0.1, lower is becoming more and more cracky
out(input.udp(host="192.168.16.80", port=5004, buffer=0.05, "audio/mpeg")

# #not working, see below #1089
# out(input.udp(host="192.168.16.80", port=5004, buffer=0.05, "audio/opus")
```

filled bug to include opus mime type and have greater features in latency and bandwidth https://github.com/savonet/liquidsoap/issues/1089

other interesting references:

- https://github.com/savonet/liquidsoap/issues/109
- https://anarc.at/services/radio/#playing-on-the-sound-card-and-live-rtp-output

## sender ffmpeg to receiver vlc

a good tradeoff between having good latency and being more crossplatform software

in sender

```
# thanks https://unix.stackexchange.com/questions/488063/record-screen-and-internal-audio-with-ffmpeg
A="$(pacmd list-sources | grep -PB 1 "analog.*monitor" | head -n 1 | perl -pe 's/.* //g')"
# thanks https://stackoverflow.com/questions/43656892/stream-opus-audio-rtp-to-android-device/48673205#48673205
ffmpeg -re -stream_loop -1 -f pulse -i default -acodec libopus -ac 1 -ab 96k -vn -f rtp rtp://10.42.0.137:5000
```

in `pavucontrol` go to *playback* tab and select input or monitor (depending on what you want to emit)

in receiver

from the sender, copy `ffmpeg` output block after `SDP:` to a file like `myfile.sdp`, in my case (as an example):

```
v=0
o=- 0 0 IN IP4 127.0.0.1
s=No Name
c=IN IP4 10.42.0.137
t=0 0
a=tool:libavformat 58.20.100
m=audio 5000 RTP/AVP 97
b=AS:96
a=rtpmap:97 opus/48000/2
```

then, open `myfile.sdp` with [VLC](https://www.videolan.org/index.es.html), it should start working

adjust preferences to quit caching (thanks https://www.softzone.es/programas/multimedia/aumentar-bufer-vlc/). Because by default there is a buffer of 1.5 seconds

- preferences / all / input/codecs / advanced / live capture caching (ms): 0 ms
- preferences / all / input/codecs / advanced / network caching (ms): 0
- preferences / all / stream output / stream output muxer caching (ms): 0 ms

## send the sound to another place (best realtime experience)

I used http://www.pogo.org.uk/~mark/trx/ with pulseaudio sound systems and 2 debian 10 laptops

clone repository:

    git clone http://www.pogo.org.uk/~mark/trx.git

dependencies required (tested in debian 10):

    sudo apt install libortp-dev libasound2-dev libopus-dev

enter directory and compile

    cd trx
    make

you need to enable realtime or you will see that *[something] is not permitted*

easiest way is to install jack

    sudo apt install qjackctl

manually, looks like is creating this file `/etc/security/limits.d/audio.conf` with this content:

```
@audio - rtprio 95
@audio - memlock unlimited
```

and maybe *if you want to enable/disable realtime permissions*, run:

    dpkg-reconfigure -p high jackd

two laptops with debian 10, on one I did:

    ./tx -h 192.168.0.32         # send audio from default soundcard to the given host

in the other

    ./rx

by default it caught my microphone, to get the *speakers* or the *emitting sound* from the other host you have to go *pavucontrol* (`sudo apt install pavucontrol`), *recording* tab and you will see *ALSA plug-in [tx]: ALSA Capture from*, change the box from *Built-in Audio Analog Stereo* to *Monitor of Built-in Audio Analog Stereo* (change appropiately with whatever soundcard you are using)
