because IRC is not dead!

This guide is done from the weechat perspective, and it helps you to connect to oftc, freenode and indymedia IRC through the most private way (tor). It also helps you to configure otr encryption which I think is the only way to encrypt 1:1 chats in IRC (and when both parts are connected). Right now, there is no way to encrypt 1:1 asynchronously, channel or group.

<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [comparison: weechat vs irssi](#comparison-weechat-vs-irssi)
- [requirements](#requirements)
- [configuration](#configuration)
- [access it through tor](#access-it-through-tor)
  - [access freenode with tor](#access-freenode-with-tor)
- [reduce our client fingerprint](#reduce-our-client-fingerprint)
- [sasl configuration (user, password)](#sasl-configuration-user-password)
- [use otr encryption](#use-otr-encryption)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

## comparison: weechat vs irssi

- codebase: irssi has a smaller codebase
- otr: found it easier to setup otr in irssi, in weechat looks like it requires more dependencies
- easy of use: in irssi you do `/connect freenode` or `/connect oftc` and you are in, in weechat you have to set up the irc servers more manually
- hidden service setup: very difficult to setup hidden service IRC with irssi (I failed [ref1](https://lists.torproject.org/pipermail/tor-talk/2014-July/033591.html) [ref2](https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO/irssi)). weechat does it in a very clean way
  - this is the thing that forced me to switch from irssi to weechat

## requirements

tested with debian 10, python dependencies are because of [otr.py](https://github.com/mmb/weechat-otr)

    apt install tor weechat-curses weechat-python weechat-plugins python-potr

weechat-python is not available in ubuntu 18.04, then use the suggested debian-ubuntu repositories by weechat: https://weechat.org/download/debian/

## configuration

The idea is to paste more or less consciously the following lines that start with a slash `/`. When you done with the configuration you applied, save all the configuration current with `/save`

## access it through tor

*connect server through tor*

create a socks5 proxy called tor:

    /proxy add tor socks5 127.0.0.1 9050

example 1: using tor to access an irc server without hidden server

add server

    /server add oftc-tor irc.oftc.net 6697

use this server through tor

    /set irc.server.oftc-tor.proxy "tor"

connect to it

    /connect oftc-tor

example 2: using tor to access an irc server that has hidden service access such as the [recommended by riseup](https://we.riseup.net/ircd/how-to-connect-through-tor-hidden-service)

    /server add indymedia-tor akeyxc6hie26nlfylwiuyuf3a4tdwt4os7wiz3fsafijpvbgrkrzx2qd.onion
    /set irc.server.indymedia-tor.proxy "tor"

extra notes:

- oftc does not have hidden service, but allows you to use tor to access it without registering a user account
- indymedia has hidden service https://we.riseup.net/ircd/how-to-connect-through-tor-hidden-service
- https://weechat.org/files/doc/stable/weechat_quickstart.en.html
- https://trac.torproject.org/projects/tor/wiki/doc/TorifyHOWTO/WeeChat

### access freenode with tor

Connecting to freenode IRC community via tor is complex, so I include the details in this guide to facilitate it.

Tor access is only provided through cert authentication. We first need to connect to freenode via plain internet

    /server add temp-freenode irc.freenode.net
    /connect temp-freenode

select the nick you want to register and verify that it is available

    /nick myIdentity
    /msg NickServ REGISTER myFancySecret mymailbox@example.com

check email, and paste the line suggested which looks like:

    /msg NickServ VERIFY REGISTER myIdentity freenodeVerificationCodeHere

keep that weechat open, and go a terminal because it is time to bind our identity with the certificate. Next commands are: create a directory for our certificate, navigate there, create a certificate (non interactively) and obtain its fingerprint (last line):

    mkdir ~/.weechat/certs
    cd ~/.weechat/certs
    openssl req -batch -x509 -new -newkey rsa:4096 -sha256 -days 1000 -nodes -out freenode.pem -keyout freenode.pem
    openssl x509 -in freenode.pem -outform der | sha1sum -b | cut -d' ' -f1

come back to weechat and copy the fingerprint to bind your user with the certificate:

    /msg NickServ CERT ADD mmyyyyCeeeerrtttiiifiiiicateee

now, add freenode server to connect via tor:

    /server add freenode-tor ajnvpgl6prmkb7yktvue6im5wiedlz2w32uhcwaamdiecdrfpwwgnlqd.onion/6697
    /set irc.server.freenode-tor.proxy "tor"
    /set irc.server.freenode-tor.ssl on
    /set irc.server.freenode-tor.ssl_cert %h/certs/freenode.pem
    /set irc.server.freenode-tor.sasl_mechanism external

I had to disable ssl verification which I think given the situation is safe https://github.com/weechat/weechat/issues/972

    /set irc.server.freenode-tor.ssl_verify off

And then you can connect to freenode IRC via tor

    /connect freenode-tor

references:

- https://freenode.net/kb/answer/certfp
- https://freenode.net/kb/answer/chat

## reduce our client fingerprint

By default, weechat client provides sensitive information that identifies us

```
/set irc.server_default.msg_part ""
/set irc.server_default.msg_quit ""
/set irc.ctcp.clientinfo ""
/set irc.ctcp.finger ""
/set irc.ctcp.source ""
/set irc.ctcp.time ""
/set irc.ctcp.userinfo ""
/set irc.ctcp.version ""
/set irc.ctcp.ping ""
/plugin unload xfer
/set weechat.plugin.autoload "*,!xfer"
```

## sasl configuration (user, password)

Some service ask you to register (the procedure could change) and then put your user password

```
/set irc.server.MyIRC-Tor.sasl_username "yourusernamehere"
/set irc.server.MyIRC-Tor.sasl_password "yourpasswordhere"
```

## use otr encryption

[otr is a cryptographic protocol that provides encryption for instant messaging conversations.](https://en.wikipedia.org/wiki/Off-the-Record_Messaging). With the following limitations:

- multi-user group chat is unsupported
- to enable it, the two parts need to be online

install it in weechat

    /script install otr.py

go privately with that person you want an encrypted communication with `/query targetUser` and then, start otr

    /otr start

then, 3 options to verify:

- verify fingerprints in real life or some sources (the more the better) and then, trust her `/otr trust`
- common secret challenge: `/otr smp ask 'ourFancySharedSecret'`
- question-answer challenge: `/otr smp ask 'What OS did we installed yesterday in our server (just the name)?' 'debian'`

check status of otr with that person

    /otr status
