# Howto do a guifi mesh network?

This guide is work in progress

This article tries to clarify how to create a mesh network and give it connectivity to guifi.net

# Types of nodes

- Node: just connects to the mesh network and helps to extend it
- Border node: provides interconnection between mesh network and the rest of the community network
- Inet (Internet) node: provides interconnection between mesh newtork and Internet

# First steps

0. Design-develop or select technology and firmware: qmp.cat, libremesh.org, gluon, temba (most close to pure openwrt), etc. Hint: It is important that in the very first nodes you provide a wan node and/or border node to make more useful the network.
1. Create a subzone
2. Assign a mesh network ip range to subzone (?)
3. Create a node inside the new subzone / Move an existing node inside the new subzone
4. Create a device inside the existing/new node and generate its mesh subnet
5. Cook firmware to antenna and configure firmware to use its mesh subnet
6. Place the antenna in the location of the new node
