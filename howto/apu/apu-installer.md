# APU Installer
Aquest repositori conté tota la informació relativa sobre el muntatge d'una màquina per fer la instal·lació massiva de les APU.
## Instal·lació
Per tal de muntar un entorn Linux minimalista per xarxa necessitem un instal·lació de Debian (buster). Un cop dins del sistema hem d'instal·lar tots els paquets necessaris per muntar el servidor PXE:

`apt-get install tftpd-hpa isc-dhcp-server debootstrap nfs-kernel-server`

Un cop instal·lats els paquets haurem de modificar els diferents fitxers de configuració:

### Interfícies de xarxa

Editem el fitxer `/etc/network/interfaces`:

```
# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto ens18
iface ens18 inet dhcp

auto ens19
iface ens19 inet static
	address 192.168.125.1
```

On `ens18` és la interfície externa i `ens19` és la interfície per a les APUs.

### DHCP
Aquí s'han de modificar 2 fitxers. Per una banda el fitxer per defecte del package `isc-dhcp-server` que es troba a `/etc/default/isc-dhcp-server`:

```
# Defaults for isc-dhcp-server (sourced by /etc/init.d/isc-dhcp-server)

# Path to dhcpd's config file (default: /etc/dhcp/dhcpd.conf).
#DHCPDv4_CONF=/etc/dhcp/dhcpd.conf
#DHCPDv6_CONF=/etc/dhcp/dhcpd6.conf

# Path to dhcpd's PID file (default: /var/run/dhcpd.pid).
#DHCPDv4_PID=/var/run/dhcpd.pid
#DHCPDv6_PID=/var/run/dhcpd6.pid

# Additional options to start dhcpd with.
#	Don't use options -cf or -pf here; use DHCPD_CONF/ DHCPD_PID instead
#OPTIONS=""

# On what interfaces should the DHCP server (dhcpd) serve DHCP requests?
#	Separate multiple interfaces with spaces, e.g. "eth0 eth1".
INTERFACESv4="ens19"
INTERFACESv6=""
```

Aquí hem afegit la interfície a la que volem fer de servidor DHCP.

Després s'ha de modificar el fitxer de configuració dels pools de DHCP que volem configurar. Podem trobar aquest fitxer a `/etc/dhcp/dhcpd.conf`:

```
# dhcpd.conf
#
# Sample configuration file for ISC dhcpd
#

# option definitions common to all supported networks...
option domain-name "rgf.cat";
option domain-name-servers ns1.example.org, ns2.example.org;
option routers 192.168.125.1;

default-lease-time 600;
max-lease-time 7200;

# The ddns-updates-style parameter controls whether or not the server will
# attempt to do a DNS update when a lease is confirmed. We default to the
# behavior of the version 2 packages ('none', since DHCP v2 didn't
# have support for DDNS.)
ddns-update-style none;

# If this DHCP server is the official DHCP server for the local
# network, the authoritative directive should be uncommented.
authoritative;

allow booting;

subnet 192.168.125.0 netmask 255.255.255.0 {
	range 192.168.125.2 192.168.125.254;
	option domain-name-servers 8.8.8.8;
	option bootfile-name "pxelinux.0";
	option domain-name "rgf.cat";
	default-lease-time 1800;
	max-lease-time 7200;
}

```

### Arrel PXE
Per tal de preparar la nova arrel del sistema que executarem via PXE necessitem executar el debotsrap:

`mkdir /pxeroot`

`debootstrap buster /pxeroot`

`chroot /pxeroot`

`apt install linux-image-amd64 nfs-common`

`mkdir /mnt/images`

Modificar el fitxer `/etc/fstab`:

```
/dev/nfs                 / nfs tcp,nolock 0 0
proc /proc    proc  defaults 0 0
none /tmp     tmpfs defaults 0 0
none /var/tmp tmpfs defaults 0 0
none /media   tmpfs defaults 0 0
none /var/log tmpfs defaults 0 0
192.168.125.1:/srv/images       /mnt/images     nfs     tcp,nolock 0 0
```

A partir d'aquí instal·larem tot el que ens sigui necessari com per exemple un servidor de ssh amb les claus públiques dels administradors...

El mountpoint de images és on estaran els volcats de disc que volguem instal·lar a les APUs.

### TFTP & SYSLINUX
Podem deixar les opcions per defecte de TFTP i haurem de copiar els fitxers `pxelinux.0` `ldlinux.c32` del netboot de Debian a `/srv/tftp`.
(http://ftp.debian.org/debian/dists/buster/main/installer-amd64/current/images/netboot/netboot.tar.gz)

També haurem d'escriure el fitxer de syslinux `/srv/tftp/pxelinux.cfg/default` amb el següent contingut:

```
DEFAULT linux

LABEL linux
    kernel vmlinuz
    append vga=normal initrd=initrd.img net.ifnames=0 root=/dev/nfs nfsroot=192.168.125.1:/pxeroot rw --

PROMPT 0
TIMEOUT 0
```

En el cas de les APU hem de bootar en mode serial i per tant haurem de passar-li al kernel els paràmetres del port serie:

```
SERIAL 0 115200
DEFAULT linux

LABEL linux
    kernel vmlinuz
    append vga=off console=ttyS0,115200n1 initrd=initrd.img net.ifnames=0 root=/dev/nfs nfsroot=192.168.125.1:/pxeroot rw --

PROMPT 0
TIMEOUT 0
```

## Posada en marxa
Per tal de poder iniciar sobre una APU hem d'executar les següents crides a la consola iPXE:

```
dhcp net0
set filename pxelinux.0
set next-server 192.168.125.1
chain tftp://${next-server}/${filename}
```

(extret de https://github.com/pcengines/apu2-documentation/blob/master/docs/apu_flashing_ipxe.md)
