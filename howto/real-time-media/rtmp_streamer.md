# rtmp nginx video stream served by your own web page

<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [Install rtmp mod for nginx](#install-rtmp-mod-for-nginx)
- [Prepare nginx for rtmp streaming](#prepare-nginx-for-rtmp-streaming)
- [Prepare the code of your web page](#prepare-the-code-of-your-web-page)
- [Serve the newly created page with nginx](#serve-the-newly-created-page-with-nginx)
- [Last steps](#last-steps)
- [Further /etc/nginx/nginx.conf configurations in the rtmp block](#further-etcnginxnginxconf-configurations-in-the-rtmp-block)
- [Other references](#other-references)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

Requirements:

* IP address reacheable at TCP ports (:80) :443 and :1935, at example.com
* nginx server
* libnginx-mod-rtmp
* OpenBroadcaster Studio or another software sending video in rtmp protocol

Sources and inspirations:

* https://live.autistici.org
* https://docs.peer5.com/guides/setting-up-hls-live-streaming-server-using-nginx/

Results:

* https://live.hangar.org
* https://live.xrcb.cat
* https://live.vitrubio.net

---

## Install rtmp mod for nginx

```bash
apt install libnginx-mod-rtmp
```

## Prepare nginx for rtmp streaming

_in this example, it is assumed that nginix default configuration serves at the address: example.com_

_Nginx can stream both in HSL and Dash/MPEG codecs, in **this example** only **HLS** is used_

in `/etc/nginx/nginx.conf`, add this block of code:

```
rtmp {
  access_log /var/log/nginx/rtmp-access.log;
  # Open nginx to listen at this port for incoming traffic
  server {
    listen 1935;
    chunk_size 4096;
    # Name your application directory
    application live {
      live on;
      # Turn on HLS
      hls on;
      hls_path /var/www/html/live.example.com/hls/;
      # finetune hls
      hls_fragment 3;
      hls_playlist_length 60;
      # Disable consuming the stream from nginx as rtmp
      deny play all;
    }
  }
}
```
check nginx and apply changes
```bash
nginx -t
nginx -s reload
```

## Prepare the code of your web page

Create /var/www/html/live.example.com and its hls/subdirectory where the video chunks and playlist will be stored:

```bash
mkdir -P /var/www/html/live.example.com/hls
```


You will need to download the following files, and place them in the /var/www/html/live.example.org directory:

* video.js  - Apache license 2 https://videojs.com/
* video-js.css  - Apache license 2 https://videojs.com/
* jquery-slim.min.js  - MIT license https://jquery.com/

You can download those files directly from an **alternative source** which is not their original but does update them from the orignal distributors:

```bash
cd /var/www/html/live.example.org/
wget https://live.hangar.org/video.js
wget https://live.hangar.org/video-js.css
wget https://live.hangar.org/jquery-slim.min.js
```
or you can download the files from the **original distributors**:

- **videojs** : We'll head to https://github.com/videojs/video.js/releases, download the latest release and use only `video-js.min.css` and `video.min.js` and the `lang/` directory

- **jquery** : We'll get the latest jquery minified from https://jquery.com/download/ and rename it to `jquery-slim.min.js`


```bash
cd /tmp/
wget https://github.com/videojs/video.js/releases/download/v7.10.2/video-js-7.10.2.zip
unzip video-js-7.10.2.zip -d video-js-7.10.2
cp video-js-7.10.2/video-js.min.css video-js-7.10.2/video.min.js /var/www/html/live.example.com/
wget https://code.jquery.com/jquery-3.5.1.slim.min.js
mv /tmp/jquery-3.5.1.slim.min.js /var/www/html/live.example.com/jquery-slim.min.js
```
create the `/var/www/html/live.example.com/index.html` file with content similar to https://live.autistici.org/index.html, https://live.hangar.org, https://live.xrcb.cat or https://live.vitrubio.net

```html
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8"/>
    <title>live.example.com</title>
    <link rel="stylesheet" href="video-js.min.css">
    <style type="text/css">
      img { border: 0px; }
    </style>
  </head>
  <body class="">
    <main class="contain">
      <header class="">
        <h1 id="title">live.example.com</h1>
      </header>
      <content class="live-streaming">
        <video id="video" class="video-js" controls preload="auto" data-setup="{}"></video>
        <div id="instructions" style="display:none;">
          <p>
              To create a new stream, pick a unique name for it, and set your broadcast
              software to stream to <i>rtmp://example.com/live/</i> with stream key <i>stream_name</i>.
          </p>
          <p>
              You will then be able to watch the live stream at
              <i>https://live.example.com/#<b>stream_name</b></i>.
          </p>
          <p>
              You can also point your favourite video player directly to
              <i>https://live.example.com/hls/<b>stream_name</b>.m3u8</i>.
              Use this url also for html embedding.
          </p>
        </div>
      </content>
      <footer class="">
      </footer>
    </main>
    <script src="jquery-slim.min.js"></script>
    <script src="video.min.js"></script>
    <script>
      $(function() {
       if (window.location.hash) {
         var name = window.location.hash.substr(1);
         console.log('playing "' + name + '"');
         $('#title').text('live.example.com - ' + name);
         var v = videojs('video');
         v.src({type: 'application/x-mpegURL', src: '/hls/' + name + '.m3u8'});
       } else {
         $('#video').hide();
         $('#instructions').show();
       }
      });
    </script>
  </body>
</html>
```

You can create your own **css style sheet** add `<link rel="stylesheet" href="assets/css/example_com-static-app.css">` in the `index.html` header after the `<link rel="stylesheet" href="video-js.min.css">` declaration and edit styles:
```bash
mkdir -p assets/css/
vim /var/www/html/live.example.com/assets/css/example_com-static-app.css
```

including for example the width for the video viewer:
```css
/*only for video-js and live streaming*/
@media screen {
  .contain{
    width:100vw;
    padding: 1rem;
  }
	.live-streaming .video-js, .live-streaming .video-dimensions {
		width: calc(30vw * 3);
		height: calc(15vw * 3);
	}
}
@media screen and (min-width:768px) {
  .contain{
    max-width:970px;
  }
	.live-streaming .video-js, .live-streaming .video-dimensions {
		width: calc(30vw * 2);
		max-width:970px;
		height: calc(15vw * 2);
		max-height:calc(970px / 2);
	}
}

```
## Serve the newly created page with nginx

_in this example, it is assumed that you previously created SSL/TLS certificates with letsencrypt_

in file `/etc/nginx/sites-available/live.example.com` and add under the `443 ssl block`:

```
    location /hls {
      # Disable cache
      add_header Cache-Control no-cache;

      # CORS setup
      add_header 'Access-Control-Allow-Origin' '*' always;
      add_header 'Access-Control-Expose-Headers' 'Content-Length';

      # allow CORS preflight requests
      if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Content-Type' 'text/plain charset=UTF-8';
        add_header 'Content-Length' 0;
        return 204;
      }

      types {
        application/vnd.apple.mpegurl m3u8;
        video/mp2t ts;
      }
    }
```

## Last steps

* check that your firewall allow incoming traffic for 1935 port
* the /var/www/html/live.example.com directory and its content are owned by www-data
* restart nginx

If you haven't done it before, you can now:

`apt install obs-studio`

and stream to `rtmp://example.com:1935/live/stream_key` and watch it live at `https://live.example.com/#stream_key`

## Further /etc/nginx/nginx.conf configurations in the rtmp block

* Nginx allow you to better finetune hls: https://nginx.org/en/docs/http/ngx_http_hls_module.html
* You can simultaneously allow clients to receive the stream in VLC player, commenting "deny play all;"
* You can simultaneously send the stream received at rtmp://example.com/live/stream_name to an external service like youtube adding: `push rtmp://a.rtmp.youtube.com/live2/stream_key;`
* You can simultaneously receive and send more streams to other locations or web pages adding other "application" blocks with different names

## Other references

- https://c3voc.de/wiki/distributed-conference


# Extra

## more than one domain, same server, different streaming

So if you want to share the server for different domains (example.com example2.com) and not mixing up the streams you should:

edit `nginx.conf`

and create different applications under `rtmp` section

As application name we'll use `input-example` and `input-example2` but you can use whatever you like, each of them pointing to different places where the stream HLS will be available.  Better to use a specific word so we try to avoid conflicts in the streaming nginx rtmp application.

```
# application directory for example.com
application input-example {
  access_log /var/log/nginx/rtmp-example-access.log;
  live on;
  # Turn on HLS
  hls on;
  hls_path /var/www/html/live.example.com/hls/;
  # finetune hls
  hls_fragment 3;
  hls_playlist_length 60;
  # Disable consuming the stream from nginx as rtmp
  deny play all;
}
# application directory for example2.com
application input-example2 {
  access_log /var/log/nginx/rtmp-example2-access.log;
  live on;
  # Turn on HLS
  hls on;
  hls_path /var/www/html/live.example2.com/hls/;
  # finetune hls
  hls_fragment 3;
  hls_playlist_length 60;
  # Disable consuming the stream from nginx as rtmp
  deny play all;
}
```

Then edit the **sites-available** and **site-enabled** with the newly created example2.com

```bash
cp -r /var/www/html/live.example.com /var/www/html/example2.com
chown -R www-data:www-data  /var/www/html/example2.com
```
and then we need to edit both `index.html` and the OBS server URL for the RMTP by replacing `live` for `input-example` and/or `input-example2`.

 ~<code>rtmp://live.example.com/live/</code>~

 `rtmp://live.example.com/input-example/`

 `rtmp://live.example.com/input-example/`


## hardcoding the stream URL

If we want to control the output of the stream, that is your **stream_key** we need to change this in the `index.html`, find the `<script>`

```js
v.src({type: 'application/x-mpegURL', src: '/hls/' + name + '.m3u8'});
```

and replace the `src:` output.

```js
v.src({type: 'application/x-mpegURL', src: '/hls/WHATEVERYOULIKE.m3u8'});
```

and our **rtmp URL** must be:  `rtmp://live.example.com/live/` and the **stream_key** will be `WHATEVERYOULIKE`

that way we can give the exact url to a third party or avoid getting abused on the server.

## translating the videojs messages

At the bottom of the index.html or where you have the call for the

```html
<script>
  $(function() {
   if (window.location.hash) {
...

   } else {
...

   }
  });
</script>
```

add this js call and content inside the first `if (window.location.hash){ }`

```html
<script>
  $(function() {
   if (window.location.hash) {

//some code here ...

        videojs.addLanguage("es",{
          "You aborted the media playback": "Has interrumpido la reproducción del vídeo.",
          "A network error caused the media download to fail part-way.": "Un error de red ha interrumpido la bajada del vídeo.",
          "The media could not be loaded, either because the server or network failed or because the format is not supported.": "La emisión no está activa en este momento.",
          "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La reproducción del video se ha interrumpido debido a que el archivo estaba corrompido o por que tu navegador no soporta este formato.",
          "No compatible source was found for this media.": "No se ha encontrado ningún origen para este medio."
        });
        videojs.addLanguage("ca",{
          "You aborted the media playback": "Heu interromput la reproducció del vídeo.",
          "A network error caused the media download to fail part-way.": "Un error de la xarxa ha interromput la baixada del vídeo.",
          "The media could not be loaded, either because the server or network failed or because the format is not supported.": "La emissió està inactiva.",
          "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La reproducció de vídeo s'ha interrumput per un problema de corrupció de dades o bé perquè el vídeo demanava funcions que el vostre navegador no ofereix.",
          "No compatible source was found for this media.": "No s'ha trobat cap font compatible amb el vídeo."
        });

//some code here ...

   } else {

//some code here ...

   }
  });
</script>
```
