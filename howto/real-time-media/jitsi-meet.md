<!-- START doctoc.sh generated TOC please keep comment here to allow auto update -->
<!-- DO NOT EDIT THIS SECTION, INSTEAD RE-RUN doctoc.sh TO UPDATE -->
**Table of Contents**

- [Quick install instructions](#quick-install-instructions)
  - [Alternate installers](#alternate-installers)
- [Useful tricks](#useful-tricks)
  - [Enter meeting directly in mode audio only](#enter-meeting-directly-in-mode-audio-only)
  - [Picture in picture](#picture-in-picture)
- [Extra configurations](#extra-configurations)
  - [Config file for webserver nginx or apache](#config-file-for-webserver-nginx-or-apache)
  - [Running jitsi in 4 GB of RAM](#running-jitsi-in-4-gb-of-ram)
  - [Ways to change default welcome page and watermark logo](#ways-to-change-default-welcome-page-and-watermark-logo)
  - [Maintaining config.json file](#maintaining-configjson-file)
    - [Disable audio levels](#disable-audio-levels)
    - [Video resolution adjustments](#video-resolution-adjustments)
    - [Adjust channelLastN](#adjust-channellastn)
    - [Start video muted parameter](#start-video-muted-parameter)
    - [Localise your jitsi instance](#localise-your-jitsi-instance)
    - [Authentication to room creation](#authentication-to-room-creation)
    - [Privacy](#privacy)
      - [Disable third party stuff](#disable-third-party-stuff)
      - [Stun turn server (optional)](#stun-turn-server-optional)
        - [Automatic coturn install](#automatic-coturn-install)
        - [Manual coturn install](#manual-coturn-install)
    - [Setup live streaming & recording with Jibri](#setup-live-streaming-recording-with-jibri)
    - [Alternate DYI live streaming & recording](#alternate-dyi-live-streaming-recording)
  - [Jitsi behind NAT](#jitsi-behind-nat)
  - [Enable stats](#enable-stats)
    - [Simple script report](#simple-script-report)
    - [Prometheus exporter](#prometheus-exporter)
- [Troubleshooting](#troubleshooting)
  - [Check coherence between hostname and /etc/hosts](#check-coherence-between-hostname-and-etchosts)
  - [Conflict in port 443](#conflict-in-port-443)
  - [Client side problems](#client-side-problems)
  - [Important server logs to inspect](#important-server-logs-to-inspect)
  - [Restarting or rebooting services](#restarting-or-rebooting-services)
  - [Jitsi videobridge consuming a lot of CPU (old bug)](#jitsi-videobridge-consuming-a-lot-of-cpu-old-bug)
  - [Disable jicofo health check (old bug)](#disable-jicofo-health-check-old-bug)
  - [Try disabling ipv6 in jvb (old bug)](#try-disabling-ipv6-in-jvb-old-bug)
- [Other docs and links](#other-docs-and-links)

<!-- END doctoc.sh generated TOC please keep comment here to allow auto update -->

This documentation refers to the [jitsi-meet service](https://jitsi.org/jitsi-meet/). [Here is the code](https://github.com/jitsi/jitsi-meet). [Here you can get updates/news](https://jitsi.org/news/) ([news RSS](https://jitsi.org/news/feed))

Deploy your own meet.jit.si service

- The team is working hard to improve the Firefox compatibility with an ETA of 1-3 weeks. -> src https://github.com/jitsi/jitsi-meet/issues/4758#issuecomment-609977084

This guide is intended to be installed in a debian 10 server (for example TLSv1.3 is not available in debian 9)

# Quick install instructions

[Follow official quick installation](https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart) (shortcut [https://jitsi.org/qi](https://jitsi.org/qi)). There is also an [official video tutorial](https://www.youtube.com/watch?v=8KR0AhDZF2A)

## Alternate installers

- maadix offers in their cloud an automatic installation of jitsi. And here is their [puppet](https://puppet.com) script https://gitlab.com/MaadiX/jitsi-meet
- ansible role by systemli https://github.com/systemli/ansible-role-jitsi-meet/
- https://yunohost.org has jitsi automatic installer that [does not work at the moment](https://github.com/YunoHost-Apps/jitsi_ynh#disclaimer)

# Useful tricks

## Enter meeting directly in mode audio only

For people with bad connection and difficulties dealing with technology you would like to send them a link that works for their use case:

https://jitsi.example.com/ferro-plataformess#config.startAudioOnly=true

## Picture in picture

Streaming producers want to have the video in a fixed position to do on their own the appropriate mix, this can be done with picture in picture with Chromium/Chrome

# Extra configurations

You need *good* HTTPS to use jitsi meet. check this guide to use them with nginx https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/nginx.md#add-letsencrypt-to-the-domain

*from now on assuming domain `jitsi.example.com`*

## Config file for webserver nginx or apache

I recommend using nginx, if this is not automatically done by *jitsi quick install*

```
cp /usr/share/jitsi-meet-web-config/jitsi-meet.example /etc/nginx/sites-available/jitsi.example.com
ln -s /etc/nginx/sites-available/jitsi.example.com /etc/nginx/sites-enabled/jitsi.example.com
nginx -t
systemctl reload nginx
```

In case you use apache2's case: here is the config file `/usr/share/jitsi-meet-web-config/jitsi-meet.example-apache`

```
cp /usr/share/jitsi-meet-web-config/jitsi-meet.example-apache /etc/apache2/sites-available/jitsi.example.com
a2ensite jitsi.example.com
apache2ctl configtest
systemctl reload apache2
```

## Running jitsi in 4 GB of RAM

By default, jitsi uses to process that consume each one 3 GB (jitsi videobridge and jicofo), [memory is never freed](https://github.com/jitsi/jitsi-meet/issues/6589) (which could be dangerous if you have extra services in the same server). If you have 4 GB of RAM you are encouraged to reduce 

- jitsi videobridge
  - /usr/share/jitsi-videobridge/jvb.sh
  - in `/usr/share/jitsi-videobridge/lib/videobridge.rc` add line `VIDEOBRIDGE_MAX_MEMORY=1024m` makes the change effective to jvb script `/usr/share/jitsi-videobridge/jvb.sh`
- jicofo
  - in `/usr/share/jicofo/jicofo.sh` change from `-Xmx3072m` to `-Xmx1024m`

These values are overridden on each `apt upgrade`

## Ways to change default welcome page and watermark logo

when you do `apt update; apt upgrade` and affects jitsi, remember that it overrides some files such as:

- `/usr/share/jitsi-meet/index.html`
- `/usr/share/jitsi-meet/images/watermark.png`

you can backup them and apply after each upgrade

or you can do it nicely this way (not tested):

- https://community.jitsi.org/t/customization-of-jitsi-meet-background-color-and-text-of-the-main-page/15635/7
- customize welcome page https://community.jitsi.org/t/customize-welcome-page/24341/5

and maybe you want to advice people uncommenting noticeMessage parameter in `/etc/jitsi/meet/jitsi.example.com-config.js`

```
    // Message to show the users. Example: 'The service will be down for
    // maintenance at 01:00 AM GMT,
    // noticeMessage: '',
```

## Maintaining config.json file

From time to time default options change, check what's new in template file `/usr/share/jitsi-meet-web-config/config.js`, and update your instance config file `/etc/jitsi/meet/jitsi.example.com-config.js`

After changes are applied, reload browser with Ctrl+F5 to clear cache

You can also look config.js from different jitsi instances on the internet, substitute accordingly: https://customjitsi.example.com/config.js

### Disable audio levels

[According to this article](https://community.jitsi.org/t/meeting-with-about-1000-participants/35005) disabling audio levels could benefit from the client side specially in large meetings. Some users do not even notice it.

Add in your config file `disableAudioLevels: true,`

### Video resolution adjustments

this is a very important thing to tweak. By default resolution is 720p, this is too much for most of the devices, specially in large rooms

some recommended 540 and others 480. That is important specially if you want to share screen in [presenter mode](https://jitsi.org/news/introducing-presenter-mode/)

did tests with 180 resolution and I see is good for the server (less bandwidth usage) and less pressure for the client side (cpu and bandwidth), the screen sharing still works at its most resolution, which is great. Here is that custom part

update 2020-4-21: some people complained that 180 resolution is too low, and [firefox resolution failed for 180 and/or 320](https://gitlab.com/guifi-exo/public/-/issues/25), so it is better to have an interval between 180 and 320 (`min: 180` and `max:320`)

```
    // Sets the preferred resolution (height) for local video. Defaults to 720.
    resolution: 320,

    // w3c spec-compliant video constraints to use for video capture. Currently
    // used by browsers that return true from lib-jitsi-meet's
    // util#browser#usesNewGumFlow. The constraints are independency from
    // this config's resolution value. Defaults to requesting an ideal aspect
    // ratio of 16:9 with an ideal resolution of 720.
    constraints: {
         video: {
             height: {
                 ideal: 320,
                 max: 320,
                 min: 180
             }
         }
     },
```

I did my own tests with:

- `glances` for the CPU measurements
  - installation: `sudo apt install python3-pip; sudo pip3 install glances`
  - run: just `glances`
- `iptraf-ng` for the bandwidth measurements
  - installation: `sudo apt install iptraf-ng`
  - run:  `sudo iptraf` press *s* (General interface statistics) and x and x to exit

this reference was useful https://community.jitsi.org/t/jitsi-dev-how-to-change-resolution-and-frame-rate-fps-of-jitsi/13883/4

### Adjust channelLastN

This variable stands for the concurrent number of remote videostreams that a user can see in her browser. By default is adjusted to value `-1` which means you are going to see them all.

The minimum possible value would be 1 and that means seeing 1 remote videostream thumbnail, hence video from another person (remote) and yourself (local). The video of the other participants are switched off with an icon that informs that the connection is *Inactive*, but you can see them clicking on their thumbnail and waiting to decode their video stream, or when the focus moves to them (if the focus is free).

You can override locally the value which could be useful for certain problematic devices or for playing with this variable more dinamically (but you require 3 devices for properly testing):

- F12
- `config.channelLastN=1` or another number
- press w two times (w is toggle tile view) *to force a change* (I don't know the appropriate way)

Tested with three participants. Looks like there is no bandwidth efficiency (that's why the image of the inactive participants comes so fast), I suspect that you will notice more in terms of cpu cycles with more participants (with 3 is very slightly)

suggested value:

```
    // Default value for the channel "last N" attribute. -1 for unlimited.
    channelLastN: 20,
```

### Start video muted parameter

This is another measure to reduce the bandwidth and keep the room light. Usually, big rooms are for presentations, so the focus is in few people. Then uncomment the following parameter

```
    // Every participant after the Nth will start video muted.
    startVideoMuted: 10,
```

communities with more privacy constraints (or very few usage of video) could ask muting all video participants by default

```
    startWithVideoMuted: true,
```

### Localise your jitsi instance

By default jitsi autodetects the language for web browsers, but maybe you want to make the user aware that this jitsi instance is located in a particular country or region

First of all, check if your language is available and translation status

In file `/etc/jitsi/meet/jitsi.example.com-config.js`

```
    // Default language for the user interface.
    // defaultLanguage: 'en',
```

uncomment default language and put one of the possibilities: https://github.com/jitsi/jitsi-meet/blob/master/lang/languages.json

for catalan's case, in `/usr/share/jitsi-meet/interface_config.js` replace variables with:

```
    DEFAULT_REMOTE_DISPLAY_NAME: 'Sense nom',
    DEFAULT_LOCAL_DISPLAY_NAME: 'jo',
```

These values in `interface_config.js` are overridden on each `apt upgrade`

### Authentication to room creation

Works https://guides.lw1.at/books/how-to-install-jitsi-meet-on-debian-or-ubuntu/page/adding-authentification-to-room-creation

- TODO test ldap config with lua-ldap  insvestigate in these sources:
  - maadix
    - https://gitlab.com/MaadiX/jitsi-meet
    - https://gitlab.com/MaadiX/jitsi-meet/-/blob/master/manifests/jitsi.pp
  - jitsi docker https://github.com/jitsi/docker-jitsi-meet

### Privacy

#### Disable third party stuff

Third party stuff example: the random funny avatars

/etc/jitsi/meet/meet.tips.es-config.js

    disableThirdPartyRequests: true,

and reload browser with Ctrl+F5 to clear cache

One of that third parties are the gravatars. [How to set avatar with gravatar.com](https://community.jitsi.org/t/how-to-set-avatar/14640). [They are open to alternatives](https://community.jitsi.org/t/how-can-i-use-avatars-from-libreavatar-org-or-my-self-hosted-instance-of-it-instead-of-gravatar/21012/2)

#### Stun turn server (optional)

Default file `/etc/jitsi/meet/jitsi.example.com-config.js` includes google stun servers for p2p situation (1 to 1 conversations)

```
// The STUN servers that will be used in the peer to peer connections
stunServers: [
    { urls: 'stun:stun.l.google.com:19302' },
    { urls: 'stun:stun1.l.google.com:19302' },
    { urls: 'stun:stun2.l.google.com:19302' }
],
```

you can run your own stun (and turn) server easily

##### Automatic coturn install

Installing coturn before jitsi quick install (`apt install coturn`) automatically configures it this way:

```
# jitsi-meet coturn config. Do not modify this line
use-auth-secret
keep-address-family
static-auth-secret=automatic secret
realm=jitsi.example.com
cert=/etc/letsencrypt/live/jitsi.example.com/fullchain.pem
pkey=/etc/letsencrypt/live/jitsi.example.com/privkey.pem


no-tcp
listening-port=4446
tls-listening-port=4445
external-ip=127.0.0.1

syslog
```

I only changed this

```diff
-cert=/etc/jitsi/meet/jitsi.example.com.crt
-pkey=/etc/jitsi/meet/jitsi.example.com.key
+cert=/etc/letsencrypt/live/jitsi.example.com/fullchain.pem
+pkey=/etc/letsencrypt/live/jitsi.example.com/privkey.pem
```

##### Manual coturn install

install coturn

    apt install coturn

enable `TURNSERVER_ENABLED=1` in /etc/default/coturn

then, in coturn's configuration file (`/etc/turnserver.conf`), add your settings.

in this sample configuration, users are not required to authenticate (no passwords, no database configuration), and use the same TLS/SSL certificates are generated with letsencrypt for jitsi.example.com itself.

```
listening-port=3478
min-port=49152
max-port=65535
fingerprint
use-auth-secret
static-auth-secret=UNIQUE_SECRET_HIDE_IT
realm=jitsi.example.com
cert=/etc/letsencrypt/live/jitsi.example.com/fullchain.pem
pkey=/etc/letsencrypt/live/jitsi.example.com/privkey.pem
cipher-list="ECDH+AESGCM:ECDH+CHACHA20:DH+AESGCM:ECDH+AES256:DH+AES256:ECDH+AES128:DH+AES:RSA+AESGCM:RSA+AES:!aNULL:!MD5:!DSS"
dh2066
log-file=/var/log/coturn.log # use "syslog" here if you prefer!
simple-log
no-tlsv1
no-tlsv1_1
```

with the secret of `static-auth-secret` variable people could use your server as a relay for media (turn server), which is required by nextcloud talk, and is optional in jitsi case

In jitsi-meet configuration (`/etc/jitsi/meet/jitsi.example.com-config.js`) change settings to:

```
stunServers: [
    { urls: 'stun:jitsi.example.com:3478' }
],
```

adjust [firewall](https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/debianVM.md#persistent-and-declarative-firewall) to allow udp/3478, in my case (iptables):

    -A INPUT -i ethX -p udp -m multiport --dports 3478 -j ACCEPT -m comment --comment "stun server (jitsi helper)"

### Setup live streaming & recording with Jibri

Not tested

Enables the ability to send recordings to dropbox or local storage and streams to youtube

Official video tutorial: [Setup live streaming & recording with Jibri](https://www.youtube.com/watch?v=OHHoqKCjJ0E)

When you have jibri already configured, the client side is very easy, you have to uncomment and enable the following options in `/etc/jitsi/meet/jitsi.example.com-config.js`, here is the section

```
    // Recording

    // Whether to enable file recording or not.
    // fileRecordingsEnabled: true,
    // Enable the dropbox integration.
    // dropbox: {
    //     appKey: '<APP_KEY>' // Specify your app key here.
    //     // A URL to redirect the user to, after authenticating
    //     // by default uses:
    //     // 'https://jitsi.example.com/static/oauth.html'
    //     redirectURI:
    //          'https://jitsi.example.com/subfolder/static/oauth.html'
    // },
    // When integrations like dropbox are enabled only that will be shown,
    // by enabling fileRecordingsServiceEnabled, we show both the integrations
    // and the generic recording service (its configuration and storage type
    // depends on jibri configuration)
    // fileRecordingsServiceEnabled: false,
    // Whether to show the possibility to share file recording with other people
    // (e.g. meeting participants), based on the actual implementation
    // on the backend.
    // fileRecordingsServiceSharingEnabled: false,

    // Whether to enable live streaming or not.
    // liveStreamingEnabled: true,
```

### Alternate DYI live streaming & recording

[obs](https://obsproject.com/) provides you more flexibility of where you want to stream it, where you want to store it and also better ways to mix it. Ideally, you will need a laptop per conference to handle that situations.

## Jitsi behind NAT

Depending on your network circunstances you could be sharing an IP with some of servers-services or you have a dedicated IP for jitsi. A dedicated public IP is the ideal situation. But if you only have one IP and you have to share between different servers continue reading here: https://github.com/jitsi/jitsi-meet/blob/master/doc/quick-install.md#advanced-configuration

in `/etc/jitsi/videobridge/sip-communicator.properties` add local and public IP address

```
org.ice4j.ice.harvest.NAT_HARVESTER_LOCAL_ADDRESS=<Local.IP.Address>
org.ice4j.ice.harvest.NAT_HARVESTER_PUBLIC_ADDRESS=<Public.IP.Address>
```

## Enable stats

In file `/etc/jitsi/videobridge/config` adapt line to `JVB_OPTS="--apis=rest, "` (extra info: https://github.com/jitsi/jitsi-videobridge/blob/master/doc/rest.md) and restart service or reboot (see below)

Check stats (install aux tools: `apt install curl jq`)

    curl -s http://127.0.0.1:8080/colibri/stats | jq -c

Extra: https://github.com/jitsi/jitsi-videobridge/blob/master/doc/statistics.md

### Simple script report

so, to have an idea, and set it up fastly, you can query the stats in different views/modes in parallel

```
#!/bin/bash

################ indented view
curl -s http://127.0.0.1:8080/colibri/stats | jq '.'
################ compact view
curl -s http://127.0.0.1:8080/colibri/stats | jq -c '.'

################ usage now
# piping jq -> src https://devops.stackexchange.com/questions/2928/how-can-i-pipe-jq-output/2931#2931
# create temp file for usage now
cat > jitsi-meet-now-usage-grep-patterns <<EOF
"conferences"
"participants"
"largest_conference"
"p2p_conferences"
"videostreams"
"videochannels"
"threads"
"largest_conference"
"endpoints_sending_audio"
EOF

echo 'usage now'
curl -s http://127.0.0.1:8080/colibri/stats | jq '.' | grep -f jitsi-meet-now-usage-grep-patterns

################ total usage
# create temp file for total usage
cat > jitsi-meet-total-usage-grep-patterns <<EOF
"total_conference_seconds"
"total_participants"
"total_conferences_completed"
"total_packets_sent"
"total_conferences_created"
EOF

echo 'total usage'
curl -s http://127.0.0.1:8080/colibri/stats | jq '.' | grep -f jitsi-meet-total-usage-grep-patterns

# delete temp files
rm jitsi-meet-now-usage-grep-patterns
rm jitsi-meet-total-usage-grep-patterns
```

### Prometheus exporter

coming soon

# Troubleshooting

So you have problems with your jitsi instance

Apply one subsection and see if it fixes it

## Check coherence between hostname and /etc/hosts

In this section, assuming that the result of `hostname` or in file `/etc/hostname` is `myHostname`

In `/etc/hosts` it should appear a line like this: `127.0.0.1 myHostname`

    getent hosts $(hostname) &> /dev/null || echo -e "error, do:\n\necho '127.0.0.1 $(hostname)' >> /etc/hosts \n"

What would happen if hostname does not match /etc/hosts?

- only chromium based browsers work (no firefox)
- two can connect if they enter in the room from different IPs
  - if enters the third video-audio fails for all the participants until the third participant exits
- two cannot connect from the same network (no video-audio)

And this is shown in `/var/log/jitsi/jvb.log` this way:

```
JVB 2020-03-20 18:08:38.186 SEVERE: [3845] util.UtilActivator.uncaughtException().122 An uncaught exception occurred in thread=Thread[IceUdpTransportManager connect thread,5,main] and message was: java.net.UnknownHostException: myHostname: myHostname: Name or service not known
java.lang.RuntimeException: java.net.UnknownHostException: myHostname: myHostname: Name or service not known
        at javax.media.rtp.rtcp.SourceDescription.generateCNAME(SourceDescription.java:40)
        at net.sf.fmj.media.rtp.RTPSessionMgr.initialize(RTPSessionMgr.java:1430)
        at org.jitsi.impl.neomedia.rtp.translator.RTPTranslatorImpl.initialize(RTPTranslatorImpl.java:913)
        at org.jitsi.impl.neomedia.rtp.StreamRTPManager.initialize(StreamRTPManager.java:226)
        at org.jitsi.impl.neomedia.MediaStreamImpl.getRTPManager(MediaStreamImpl.java:2081)
        at org.jitsi.impl.neomedia.MediaStreamImpl.createSendStreams(MediaStreamImpl.java:966)
        at org.jitsi.impl.neomedia.MediaStreamImpl.startSendStreams(MediaStreamImpl.java:3175)
        at org.jitsi.impl.neomedia.MediaStreamImpl.start(MediaStreamImpl.java:3064)
        at org.jitsi.impl.neomedia.MediaStreamImpl.start(MediaStreamImpl.java:3027)
        at org.jitsi.videobridge.RtpChannel.maybeStartStream(RtpChannel.java:1121)
        at org.jitsi.videobridge.Channel.transportConnected(Channel.java:947)
        at java.lang.Iterable.forEach(Iterable.java:75)
        at org.jitsi.videobridge.IceUdpTransportManager.onIceConnected(IceUdpTransportManager.java:1960)
        at org.jitsi.videobridge.IceUdpTransportManager.lambda$startConnectivityEstablishment$5(IceUdpTransportManager.java:2175)
        at java.lang.Thread.run(Thread.java:748)
Caused by: java.net.UnknownHostException: myHostname: myHostname: Name or service not known
        at java.net.InetAddress.getLocalHost(InetAddress.java:1506)
        at javax.media.rtp.rtcp.SourceDescription.generateCNAME(SourceDescription.java:37)
        ... 14 more
Caused by: java.net.UnknownHostException: myHostname: Name or service not known
        at java.net.Inet6AddressImpl.lookupAllHostAddr(Native Method)
        at java.net.InetAddress$2.lookupAllHostAddr(InetAddress.java:929)
        at java.net.InetAddress.getAddressesFromNameService(InetAddress.java:1324)
        at java.net.InetAddress.getLocalHost(InetAddress.java:1501)
        ... 15 more
```

## Conflict in port 443

Jitsi videobridge tries to use port 443 if available, which could happen when you are preparing your nginx or apache webserver

If you see 443 is by a java, that's the jitsi videobridge

    ss -tupanl | grep 443

Then, do:

    systemctl stop jitsi-videobridge2

And ensure your webserver starts in 443

Then:

    systemctl start jitsi-videobridge2

## Client side problems

Jitsi relies heavily on javascript. If you are experimenting problems, check that the following addons are bypassed: adblock plus, ublock, ublock origin, noscript, privacy badger, etc.

Additionally, check javascript console in client browser for errors going undetected by server

## Important server logs to inspect

- client side: check javascript console in client browser for errors going undetected by server
- server side, important errors comes with SEVERE tag in the beginning
  - /var/log/jitsi/jicofo.log - jitsi meet (javascript, and binding to videobridge and prosody)
  - /var/log/jitsi/jvb.log - jitsi videobridge
  - /var/log/prosody/prosody.log - XMPP server (chat, room allocation)

## Restarting or rebooting services

Do it in this order

    systemctl restart jicofo
    systemctl restart jitsi-videobridge2
    systemctl restart prosody

If still is not working, after some time:

    systemctl restart jicofo

## Jitsi videobridge consuming a lot of CPU (old bug)

This issue https://community.jitsi.org/t/jitsi-videobridge-consuming-200-cpu-utilization-constantly/19482

looks like is not applying anymore for jitsi-videobridge2 because is not creating the tcp socket listener

## Disable jicofo health check (old bug)

*this is from an old bug that is probably fixed*

/etc/jitsi/jicofo/sip-communicator.properties

    org.jitsi.jicofo.HEALTH_CHECK_INTERVAL=-1

and restart jicofo

    service jicofo restart

Why?

[As said by a jitsi developer](https://github.com/jitsi/jitsi-meet/issues/2172#issuecomment-389589242) health checks are for environments with multiple jvbs instances [1 How to Load Balance Jitsi Meet](https://www.youtube.com/watch?v=LyGV4uW8km8) [2 Scaling Jitsi Meet in the Cloud Tutorial](https://www.youtube.com/watch?v=Jj8a6ZRgehI) (this is the scenario of meet.jit.si, with multiple instances around the world attending the same service); in other words, there is no sense to have the health check for a single jvb instance. [The problem](https://github.com/jitsi/jitsi-meet/issues/2172) (at least with a single jvb instance) is that the healthcheck opens supposedly too much UDP ports that are not immediately closed after the healtcheck, hence, jicofo crashes and "jitsi is not working"

Update 2020-4-3: with this enabled these days works; and having health check helps the service to be up

## Try disabling ipv6 in jvb (old bug)

*this is from an old bug that is probably fixed*

some time ago, there was a bug with ipv6 that made jitsi to fail. In file `/etc/jitsi/videobridge/sip-communicator.properties` add `org.ice4j.ipv6.DISABLED=true`

then, restart or reboot

# Other docs and links

- Install jitsi https://gitlab.com/guardianproject-ops/jitsi-aws-deployment
- Jitsi installation guide https://jitsi.github.io/handbook/docs/devops-guide/devops-guide-quickstart
- Jitsi user guide done by mayfirst.org [en](https://support.mayfirst.org/wiki/web-conference) [es](https://support.mayfirst.org/wiki/web-conference.Es)
- Jitsi Videobridge documentation - https://github.com/jitsi/jitsi-videobridge/blob/master/doc
- Jitsi instances lists:
  - [Instances according to jitsi wiki](https://github.com/jitsi/jitsi-meet/wiki/Jitsi-Meet-Instances)
  - [Jitsimeter Which instance of Jitsi should I use?](https://ladatano.partidopirata.com.ar/jitsimeter/)
  - https://fediverse.blog/~/DonsBlog/videochat-server
- [Compares different videoconference services (basque)](https://librezale.eus/wiki/KomunikazioEtaElkarlanerakoTresnak)
- Signaling issues will be gone, i.e., one of the participants not receiving audio/video. -> src https://github.com/jitsi/jitsi-meet/issues/4758#issuecomment-604463127
- [Problems of p2p video conferences](https://github.com/nextcloud/spreed/#scalability). It also explains that nextcloud talk has a propietary solution for SFU only available for corporate subscriptions.
  - [what is SFU?](https://webrtcglossary.com/sfu/)
- [Jitsi explains crisis covid-19](https://youtu.be/APVp-20ATLk?t=876)
- Question: jitsi over tor? answer: jitsi, as mostly all solutions these days, are using webrtc. WebRTC it is bad for the use case of hiding your original ip
  - here says how your ip is revealed https://tor.stackexchange.com/questions/876/does-tor-work-with-webrtc
  - this comment says maybe it's possible (so this is in research status) https://trac.torproject.org/projects/tor/ticket/16221#comment:22
- Guide about how to use jitsi (catalan) https://formacio.cesire.cat/AulaVirtual/Jitsi/
- Guide for the free Conference Server of Freifunk Munich https://ffmuc.net/wiki/doku.php?id=knb:meet-en
  - Meet stats https://stats.ffmuc.net/d/U6sKqPuZz/meet-stats
- [security] In jitsi, data is decrypted in the server, but you can run your own instance https://github.com/jitsi/jitsi-meet/blob/master/README.md#security
- [security] catalan police lied about jitsi https://jitsi.org/the-mossos-confusion/
- Freifunk München - FFMEET https://www.youtube.com/watch?v=v_ocoV7d7KI&feature=youtu.be
  - prosody, switch to `network_backend = "epoll"` https://community.jitsi.org/t/prosody-1024-limit/24396/5
  - nginx: raise number of nginx workers, raise max open files limit
- Another guide https://guides.lw1.at/books/how-to-install-jitsi-meet-on-debian-or-ubuntu
