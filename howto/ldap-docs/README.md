# Instal·lació

Paquets a instal·lar:

```
sudo apt install slapd ldap-utils
```

un cop instal·lat fer

```
sudo dpkg-reconfigure slapd per actualitzar totes les dades
```

1. usarem el nostre domini i nom
2. la bdd serà MDB
3. Comprovem que el servei estigui corrent

## Accés al directori de configuració

<https://tech.feedyourhead.at/content/openldap-set-config-admin-password>

Creem la contrasenya xifrada:

```
sudo slappasswd -s elpassquevolem
```

ens dona:

```
{SSHA}FIlZBIahdorp8QOCPkjXa84WbWMc
```

creem el fitxer add*admin*config.ldif amb:

```
dn: olcDatabase={0}config,cn=config
changetype: modify
add: olcRootPW
olcRootPW: {SSHA}NTc52ahBM+u/kXNkXUwI6gYuZVc
```

per carregar el fitxer:

```
sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f add*admin*config.ldif
```

per modificar el pass:

```
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: {SHA}iDNpwXtVU6h8uaPzjj2JwJc=
```

**Nota**: tots els certificats que apareixen son a mode d'exemple.

## Directoris per defecte

config `/etc/ldap/sladp.d`

schema de configuració `/etc/ldap/schema`

directori de l’aplicació `/usr/lib/ldap`

# memberOf

MemberOf es un overlay que es pot carregar sobre openLDAP i permet afegir l’identificació de membres que pertanyen a un element d’OU tipus groupOfNames

Usarem aquesta funcionalitat per assignar privilegis d'accés a les aplicacions de l’associació.

Per instal·lar-la seguirem:

<https://tylersguides.com/guides/openldap-memberof-overlay/>

Afegim el mòdul memberof i el mòdul refint per garantir la integritat referencial entre els grups(aplicacions) i els usuaris

Primer, afegirem els modus a `cn=module{0},cn=config`

```
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: memberof.la
dn: cn=module{0},cn=config
changetype: modify
add: olcModuleLoad
olcmoduleload: refint.la
```

Es carregaran amb:

```
ldapmodify -Q -Y EXTERNAL -H ldapi:/// -f 1-schemaUpdate.ldif
```

Segon, afegirem la definició dels dos overlays:

```
dn: olcOverlay=memberof,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcMemberOf
objectClass: olcOverlayConfig
objectClass: top
olcOverlay: memberof
olcMemberOfDangling: ignore
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfNames
olcMemberOfMemberAD: member
olcMemberOfMemberOfAD: memberOf
dn: olcOverlay=refint,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcRefintConfig
objectClass: top
olcOverlay: refint
olcRefintAttribute: memberof member manager owner
```

Es carregaran amb:

```
ldapadd -Y EXTERNAL -Q -H ldapi:/// -f 2-schemaNew.ldif
```

# Apache Directory Studio

Descarrega:

<https://directory.apache.org/studio/download/download-linux.html>

Es descomprimeix i es mou al directori des del que el volem executar. Es una aplicación java

s’executa com `./ApacheDirectoryStudio &`

Asegurar que ldap está corrent

Accés a la branca de configuració:

Amb Apache studio crear una conexión nova amb File -> new -> LDAPBrowser  -> LDAP connection

El nom l’hi posem exoprod-config per saber que es el config:

1. Next ->
   1. Bind DN cn=admin,cn=config i el pass que hem creat
2. Next ->
   1. Base DN: cn=config

Accés a la branca de dades:

El nom l’hi posem exoprod-dades per saber que es el config

1. Next ->
   1. Bind DN cn=admin,dc=exo,dc=cat i el pass que hem creat
2. Next ->
3. finish

# Apunts sobre LDAP

LDAP usa el format LDIF per entrar l’informació

El format LDIF sempre comença per:

dn: nom de l'objecte (sempre és únic) Sempre serà al camí sencer.

Per exemple:

```
dn: ou=usuaris,dc=exo,dc=cat
```

Primer caldrà crear objectes ou que es una organization unit (equivalent a una carpeta)

Per exemple, pels usuaris crearem

```
dn: ou:usuaris,dc=exo,dc=cat
objectclass: top
objectclass: organizationalUnit
ou: usuaris
```

Sempre és aconsellable deixar una línia en blanc al final del fitxer

es poden crear varis objectes un darrere l’altre deixant una linia en blanc entre cada definició

Per carregar el fitxer usarem:

```
sudo ldapadd -x -W -D ‘cn=admin,dc=exo,dc=cat’ -H ldap://localhost -f fitxer.ldif
```

Si no afegim -f fitxer.ldif entrarem en una sessió LDAP i podrem anar carregant les línies dels fitxers amb un copy & paste

el intro ens executarà el paquet de línies que haguem entrat i podrem seguir afegint més elements

Les funcions bàsiques de ldap son:

`ldapadd`, `ldapmodify`, `ldapdelete` i `ldapseach`

```
sudo ldapsearch -x -b 'dc=exo,dc=cat' -H ldap://localhost '(uid=marta\*)'
```

Si es vol fer auditoria dels canvis de configuració es pot

Per consultar els camps disponibles:

<https://www.zytrax.com/books/ldap/ch3/>

<https://ldapwiki.com/wiki/Main>

Per poder analitzar els logs

<http://ipswitchmsg.force.com/kb/articles/FAQ/How-to-configure-OpenLDAP-logging-1307739574654>

```
sudo slapd -d -1
```

Si es debuga amb root i es modifiquen els permisos cal:

* sudo chown openldap.openldap /var/run/slapd/\*
* sudo chown -R openldap.openldap /etc/ldap/slapd.d/

Si ens hem equivocat amb l’estructura, podem eliminar un mòdul de cn=config segons

<https://www.zytrax.com/books/ldap/ch6/slapd-config.html#use-schemas>

Per canviar el password del admin

<https://www.digitalocean.com/community/tutorials/how-to-change-account-passwords-on-an-openldap-server>

<https://serverfault.com/questions/438777/how-do-you-set-the-admin-password-on-openldap-2-4>

```
slappasswd -h {SSHA}
```

entrem el nou pass i ens dona el pass en ssha

```
sudo ldapmodify -Y EXTERNAL -H ldapi:///
```

```
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootPW
olcRootPW:{SSHA}j444pV8e1aRk/Ls9PvwDkafFIaatOKXy
```

# Configuració de l’estructura

## Definició de les unitats organitzatives

L’estructura creada consta de les següents OU:

```
dn: ou=usuaris,dc=exo,dc=cat
objectclass: top
objectclass: organizationalUnit
ou: usuaris
dn: ou=grups,dc=exo,dc=cat
objectclass: top
objectclass: organizationalUnit
ou: grups
dn: ou=serveis,dc=exo,dc=cat
objectclass: top
objectClass: organizationalUnit
ou: serveis
```

## Usuaris

Usuaris: a on ens guarden tots els usuaris de les aplicacions

Grups: per aplicar privilegis a nivell d’aplicació

Serveis: per donar accés dels socis a les aplicacions

Els usuaris generics pels serveis els anomenarem ldapServei, per exemple ldapNextcloud

Crearem inicialment:

* ldapdolibarr
* ldapnextcloud
* ldapwordpress

## Grups

Públic: per persones amb accés a les nostres plataformes pero que no son membres de l’associació. Per exemple, un client.

Associació: membres de l’associació

Gestió: membres de l’associació amb tasques de gestió.

Els grups creats son:

```
dn: cn=public,ou=grups,dc=exo,dc=cat
objectclass: top
objectClass: posixGroup
cn: public
gidNumber: 30000
description: "Grup pel public"
dn: cn=associacio,ou=grups,dc=exo,dc=cat
objectclass: top
objectClass: posixGroup
cn: associacio
gidNumber: 30001
description: "Grup pels membres de l'associació"
dn: cn=gestio,ou=grups,dc=exo,dc=cat
objectclass: top
objectClass: posixGroup
cn: gestio
gidNumber: 30002
description: "Grup pel usuaris de gestio”
```

## Serveis

A continuació i ha un exemple de la creació de les aplicacions i un usuari assignat.

*dn: cn=nextcloud,ou=serveis,dc=exo,dc=cat*

*objectclass: top*

*objectClass: groupOfNames*

*cn: nextcloud*

*member: uid=ldapnextcloud,ou=usuaris,dc=exo,dc=cat*

*dn: cn=dolibarr,ou=serveis,dc=exo,dc=cat*

*objectclass: top*

*objectClass: groupOfNames*

*cn: dolibarr*

*member: uid=ldapdolibarr,ou=usuaris,dc=exo,dc=cat*

*dn: cn=wordpress,ou=serveis,dc=exo,dc=cat*

*objectclass: top*

*objectClass: groupOfNames*

*cn: wordpress*

*member: uid=ldapwordpress,ou=usuaris,dc=exo,dc=cat*

*dn: cn=ldapadmin,ou=serveis,dc=exo,dc=cat*

*objectclass: top*

*objectClass: groupOfNames*

*cn: ldapadmin*

*member: uid=ldapdolibarr,ou=usuaris,dc=exo,dc=cat*

*member: uid=ldapnextcloud,ou=usuaris,dc=exo,dc=cat*

*dn: cn=ldapread,ou=serveis,dc=exo,dc=cat*

*objectclass: top*

*objectClass: groupOfNames*

*cn: ldapread*

*member: uid=ldapwordpress,ou=usuaris,dc=exo,dc=cat*

# Flux de les dades

El flux de les dades es el següent:

![FluxDades.jpg](./FluxDades.jpg)

Des de tresoreria es crea un nou soci.

Com Dolibarr sols permet sincronitzar socis amb LDAP i no tercers, es mandatori crear sempre el soci.

Per distingir si serà soci o no, crearem un tipus de soci adicional que serà tercer sense soci.

A aquest tipus de soci, no es crearà la plantilla per la facturació de la quota de soci.

Des de gestió tècnica, s'assignarà mitjançant Apache Directory Studio o qualsevol altre gestor de openLDAP els serveis que dispondrà el nou soci asignant-ho a la OU=serveis

També es cambiar el password a crypt-sha-256

Les aplicacions/serveis automàticament crearan els usuaris per a cada servei que s’asigni dins de LDAP.

Per poder canviar el pass des de nextcloud:

A la pestanya avançat:

Marcar: Activa el canvi de contrasenya LDAP pels usuaris

Per que aparegui l’opcio de recuperar el pass des amb connexió a LDAP

<https://help.univention.com/t/forgot-password-on-nextcloud-login-page/13660>

# Aplicacions

## Nextcloud

Si no està instal·lat, instalar apt install php-ldap

Reiniciar apache

Activar el paquet de Integració LDAP i configurarem:

Connexió al servidor:

![](https://lh4.googleusercontent.com/Gn5tJdDnSJJbujVDQMEC8kHaWzUH6HF5CAYeorHy5D5vsk0-dsaEAcORbq-WIcrM9yfzz1a9H41D6R2zh4WpcpXIP-lfKrdYMrDqyX-CaLJvbUUCA9Hu1SjjduPcDCsQDZn52HzM)

Host: 192.168.59.23

Port: 389

DN: uid=ldapnextcloud,ou=usuaris,dc=exo,dc=cat

DN Base= dc=exo,dc=cat

Important: Guardar les credencials

Filtre dels usuaris:

![](https://lh5.googleusercontent.com/TGMVLrL85UJ0Jw02M8YBkieCLVhe0elzvsWdYJ8ghINL3I-Q6S8FzOl5cw0TsmKpFMDW-WfeZllUwGlq6ZXLbNnThlzgZyXWWvPh6nTteLBs94h4_DbkxKUM_7gVNy4-h3lUewjd)

Only these object classes:

En aquest punt és a on estem aplicant el filtre memberof cal editar la LDAP Query

(&(objectClass=inetOrgPerson)(memberOf=cn=nextcloud,ou=serveis,dc=exo,dc=cat))

Atributs d’accés:

![](https://lh4.googleusercontent.com/_FmaeBbKPjeNRxNZ4jkFAZkKo7_X9JFfg1PgG8Aqb1v2o8nQ9NhiUTmh-TzMw7VrfzTLycvsLrcOOTptllVmb7N0yVu_zPJYZ1CTjj5fOri9vOn85Ijnz6vDFn0KpPkVBO-Zq-yF)

Usarem el camp uid com identificador

En aquesta solapa podem assignar més paràmetres a l’aplicació des de LDAP en funció de les propietats que definim en els usuaris.

FIltre LDAP: (&(&(objectClass=inetOrgPerson)(memberOf=cn=nextcloud,ou=serveis,dc=exo,dc=cat))(|(uid=%uid)(|(gidNumber=%uid))))

Grups:

![](https://lh5.googleusercontent.com/c0QcRx4XSZNpC8pOD1TpKFDdTR3AhwUIE1HPUKS2u8bbogadpiU3yw5ipe_NWLEU_E3CvO_Va7gSn7ZP_J1-RqzqwWPMsgcw-eIcMERdisLjLkhknu5GWQi2UiSZWSBMtj0E52eT)

Indiquem quin grups de LDAP s’inclouran a NextCloud

<https://github.com/nextcloud/server/issues/5406>

Per canviar el identificador intern pel uid anar a la opció expert

Atribut nom d'usuari intern: uid

Atribut UUID per Usuaris: uid

Pulsar el botó Elimina mapage usuari

Per permetre el canvi de password, cal marcar a Advanced:

Desplegar Directory Settings

Enable LDAP password changes per user

Per que com a nom sols surti el sn, cal marcar a Advanced:

Desplegar Directory Settings

Camp per mostrar el nom d'usuari: sn

## Dolibarr

Cal afegir els següents paquets: php-ldap

**apt install php-ldap**

A /etc/php/7.3/php.ini habilitar ldap

descomentar extension=ldap

A /etc/php/7.3/apache2/php.ini modificar:

upload*max*filesize (150M)

post*max*size (150M)

Reiniciar apache

En primer lloc s’haurà d’activar el mòdul de LDAP

Per configurar la connexió a server de LDAP:

![](https://lh6.googleusercontent.com/WGv5elWWjZs8obGA0ioQEKfVBh-CwABN66oCIVVKk5LiZFxzEEAGXzujvdcUUEuLc_jVSeDS6zOYB5Vm_V5SwwnlkE-PWUFGm-KOkinxIacYovPj4OxxAJCvu4U7vAckuEsko33s)

Sincronització d'usuaris i grups: de LDAP -> Dolibarr

Sincronització de socis: Dolibarr -> LDAP

DN del servidor: uid=ldapdolibarr,ou=usuaris,dc=exo,dc=cat

Usarem el filtre memberOf per que sols surtin els usuaris i grups que corresponguin a Dolibarr.

&(objectClass=inetOrgPerson)(memberOf=cn=dolibarr,ou=serveis,dc=exo,dc=cat)

Configuració per Usuaris:

![](https://lh4.googleusercontent.com/pXKyXgChFtLy-UaVsU6kz-RB1EukU6Sz9rGg7Afy-WK8GiS1qXhX8r8IlBbTgtgQlAeonfnAn3SuN43NUgRxoHUa3e8zBBifWWov8JEvI-MwYT2KBz62wu_KLYJ4WUvEjE-om87L)

DN dels usuaris: ou=usuaris,dc=exo,dc=cat

Llista de objectClass: inetOrgPerson, posixAccount,shadowAccount

Filtre de cerca: &(objectClass=inetOrgPerson)(memberOf=cn=dolibarr,ou=serveis,dc=exo,dc=cat)

![](https://lh3.googleusercontent.com/FEc_aFBtId3RP3efmODk3QCOB69pKlqG8Juod968IBE-dKW4NWZY-hbu9Q84PfCGZxYFH0aLudfaCAtbj-d_HuFus1LBtmg2lRyUyEAqdkziDWmVk4gLQLpZTFDPW96mQrl5W_ll)

Nom complet: cn

Nom: sn

Login (unix): uid

login: uid

Contrasenya encriptada: No s'ha de sincronitzar

Email: mail

La resta en blanc

Clau ldap: uid (Login (unix))

Configuració per Grups:

![](https://lh4.googleusercontent.com/nIldpZQI9oEnvSc1vVXFYAUef97YaQPjAtQ8e9iYG3VNQsrkXlItHY2jeNjF4OHl7OMq6GNgrA4EJKbUo_5IN1z3FY0d3_jN6PUaY28nrLfZUluSbX2iKnxdXFF2R0xNkkFZH1--)

DN dels grups: ou=grups,dc=exo,dc=cat

Llista de objectClass: posixGroup

Nom: cn

Descripció: description

Socis del grup: gidNumber

Clau ldap: cn

Socis:

![](https://lh5.googleusercontent.com/qxIq_x1IG1ks0tKnc2CblJT4E3z6_ZJybUTsJw4BdBY4x-NLCVmM0pZj8_a7DHqLhRJCjCnn1mOibzi7QZ_amavySKslffWIiWwDNjO9azf42TCmnQQuJfBYvrpieoYtHB8QVXtd)

DN dels socis de Dolibarr: ou=usuaris,dc=exo,dc=cat

Llista de objectClass: inetOrgPerson,posixAccount,shadowAccount

Filtre de cerca: &(objectClass=inetOrgPerson)

![](https://lh6.googleusercontent.com/GAKTmhgGTaukK3_MeQ0DpP0XLcbcRccVkTQbQlEsS0Q4vA8jjaISJuRXQ3W8el54y7xX73vss2ee8Fo_ifI9lSqEAU2SWsuRueyzwqXP5D50GQVj1tftCmBIfXdtoAK52yPN1Vvm)

![](https://lh4.googleusercontent.com/UG9anILqeaGd2y86513jcoA1YHVIoHWk5Uy7qkJz3q0ax9Ko_gqTlmYfFeY-Kp7tl9SBa20b57Cn_sNzFFHq39ahO_CFA3HqCr-mIZLtxeIb4EA25oqCWnR7rEHNBU0lEqagb3Y1)

Nom complet: cn

Nom:

Nom: sn

Login(unix): uid

login: homeDirectory

Contrasenya encriptada: userPassword

Identificador d'usuari(uidNumber): uidNumber

Identificador de grup(gidNumber): gidNumber

Nota publica: description

Clau ldap: uid

Perquè surtin els text correctes, a Configuració -> Traducció afegirem entrades noves:

![](https://lh5.googleusercontent.com/_zGhrD6b0ViLrMJLuBi4Mdx55mRcncPOxXy6a-0ZPi9Yn-o9mqvgaCk9kw_eQXXgplptaKp1s19-R-mqcggVxHJeryNY9gSgXUShP3bdiBJ7cMiY4FJDJUZsas591X9pegafDFWw)

![](https://lh6.googleusercontent.com/n2Ss-vXEErD506VuEOBupjFeF0mJNBHz1IuanE5K3XfVGAaeAIKPNMs3VvYI6da7LYFXGx6yl8hPDwNp-hQk6vNibl_9wi0_dqq7M80teZJE0VsAxw5emUlW4UEN0C5PfevBUSJA)

En el fitxer /etc/dolibarr/conf.php afegir les següents línies:

$dolibarr*main*auth*ldap*host='127.0.0.1';                  // You can define several servers here separated with a comma.

$dolibarr*main*auth*ldap*port='389';                        // Port

$dolibarr*main*auth*ldap*version='3';

$dolibarr*main*auth*ldap*servertype='openldap';           // openldap, activedirectory or egroupware

$dolibarr*main*auth*ldap*login_attribute='uid';       // Ex: uid or samaccountname for active directory

$dolibarr*main*auth*ldap*dn='ou=usuaris,dc=exo,dc=cat'; // Ex: ou=users,dc=my-domain,dc=com

$dolibarr*main*auth*ldap*admin_login='uid=ldapdolibarr,ou=usuaris,dc=exo,dc=cat';           // Required only if anonymous bind disabled. Ex: cn=admin,dc=example,dc=com

$dolibarr*main*auth*ldap*admin_pass=elmeupassdeldap';               // Required only if anonymous bind disabled. Ex: secret

$dolibarr*main*auth*ldap*debug='false';

$dolibarr*main*auth*ldap*filter = '(&(uid=%1%)(memberOf=cn=dolibarr,ou=serveis,dc=exo,dc=cat))'; // If defined, two previous parameters are not used to find a user into LDAP.

Ex: (uid=%1%) or &(uid=%1%)(isMemberOf=cn=Sales,ou=Groups,dc=opencsi,dc=com).

També cal canviar

$dolibarr*main*authentication='dolibarr,ldap';

per afegit la autenticació per ldap

El password ha de quedar guardat a ldap com CRYPT-SHA-256 hashed password

per que dolibarr l’entengui

Crear una tasca cron cada 5 minuts amb:

php sync*users*ldap2dolibarr.php nocommitiferror -y

*/5* \* php /usr/share/dolibarr/scripts/user/sync*users*ldap2dolibarr.php commitiferror --server=testexo.cat -y >> /dev/null 2>&1

Per crear les dades dels socis existents per que es pugui pujar cap a ldap caldrà un procediment sql per fer:

PhonePerso(phone_perso) -> Identificador de grup(gidNumber): gidNumber = 30001 (associació)

PhonePro(phone) -> Identificador d'usuari(uidNumber): uidNumber = ref del soci(rowid) + 20000

login(Unix) -> uid = nom + cognom de manera que sigui facil identificar els socis

password -> password = qualsevol valor

*UPDATE public.llx_adherent*

*SET (phone_perso, phone) = (30001, (rowid + 20000));*

*UPDATE public.llx_adherent*

*set login = case when position(' ' in lastname)=0*

*then  trim(both from lower(concat(REPLACE(firstname,' ',''),lastname)))*

*else  trim(both from lower(concat(REPLACE(firstname,' ',''),SUBSTRING(lastname,1,position(' ' in lastname))))) end;*

*UPDATE public.llx_adherent*

*SET pass = '\*\*\*\*\*\*';*

Eliminar manualment accents, ñ, ç i qualsevol altre caràcter que no sigui lletres ASCII pures del login

per sincronitzar tots els socis existents cap a LDAP ejecutar el script del directori /usr/share/dolibarr/scripts/members :

./sync*members*dolibarr2ldap.php commitiferror --server=testexo.cat -y |grep KO

El grep serveix per que es mostrin sols el errors, per si ens hem deixat un login erroni

Per asignar automáticament el uidNumber, crearem una funció associada el trigger del update

Funció del trigger:

Es crea al schema sota trigger functions.

Nom uidNumberAssign

Codi:

*BEGIN NEW.phone = NEW.rowid + 20000; RETURN NEW; END;*

Trigger:

Es crea a la taula llx_adherent

Nom: uidNumberCreate

Row trigger

Fires: before update

trigger function: uidNumberAssign

# 

# ACL’s

<https://www.openldap.org/doc/admin24/access-control.html>

<https://www.linuxarena.net/2018/03/01/isc-dhcp-con-backend-ldap-parte-iv/>

<https://proyectos.interior.edu.uy/projects/sauce/wiki/Permisos_y_Grupo_en_Openldap>

[https://www.openldap.org/doc/admin24/guide.html#Basic ACLs](https://www.openldap.org/doc/admin24/guide.html#Basic%20ACLs)

<https://medium.com/@moep/keeping-your-sanity-while-designing-openldap-acls-9132068ed55c>

### **Valors** per defecte

En l’entrada {-1} frontend

*{0}to by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth manage by break*

*{1}to dn.exact="" by \* read*

*{2}to dn.base="cn=Subschema" by \* read*

En entrada {0} config

*{0}to by dn.exact=gidNumber=0+uidNumber=0,cn=peercred,cn=external,cn=auth manage by break*

En entrada {1} mdb

*{0}to attrs=userPassword by self write by anonymous auth by \* none*

*{1}to attrs=shadowLastChange by self write by \* read*

*{2}to by read*

Per veure les regles existents

ldapsearch -Q -LLL -Y EXTERNAL -H ldapi:/// -b cn=config '(olcAccess=\*)' olcAccess olcSuffix

Per carregar els permisos d'accés

sudo ldapmodify -Y EXTERNAL -H ldapi:/// -f ./olcAccess.ldif

sudo ldapsearch -x -b 'dc=exo,dc=cat' -D 'uid=josepLdap,ou=usuaris,dc=exo,dc=cat' -w bondia100$ '(uid=marta\*)'

sintaxis:

olcAccess: que [ressource]

by [who] [type d'accès autorisé]

by [who] [type d'accès autorisé]

by [who] [type d'accès autorisé]

### **Who to grant access to**

The <who> part identifies the entity or entities being granted access. Note that access is granted to "entities" not "entries." The following table summarizes entity specifiers:

**Specifier**

**Entities**

\*

All, including anonymous and authenticated users anonymous

Anonymous

(non-authenticated) users

users

Authenticated users

self

User associated with target entry

dn[.<basic-style>]=<regex>

Users matching a regular expression

dn.<scope-style>=<DN>

Users within scope of a DN

### **The access to grant**

The kind of <access> granted can be one of the following:

**Level**

**Privileges**

**Description**

none

=0

no access

disclose

=d

needed for information disclosure on error

auth

=dx

needed to authenticate (bind)

compare

=cdx

needed to compare

search

=scdx

needed to apply search filters

read

=rscdx

needed to read search results

write

=wrscdx

needed to modify/rename

manage

=mwrscdx

needed to manage

Basics:

access to attrs=userPassword

by self write

by anonymous auth

by \* none

access to \*

by self write

by users read

by \* none

access to \*

by anonymous none

by \* read

Específics:

access to dn.children="dc=exo,dc=cat"

by group.exact="cn=ldapAdmin,ou=ldapGrups,ou=grups,dc=exo,dc=cat" write

by \* auth

### **Procediment d’avaluació**

La lista de acceso es una línea de la forma: “**<what> <who> <access> [control := (stop | continue | break)]**”.

El control de acceso del demonio ‘slapd’ compara la entrada sobre la que se está accediendo con el “<what>” hasta que encuentra una coincidencia. Cuando es encontrada, recorre su lista de acceso hasta que el primer “who” coincide con el usuario que está accediendo. Entonces aplica el acceso que contiene.

Si no se define ninguna lista de acceso para una base de datos, entonces la lista de acceso implícita para esa base de datos es: *access to by read;*

Por el contrario si se define al menos una oclAccess para esa base de datos, entonces la última lista de acceso implícita para esa base de datos es: *access to by none;*

Es una buena estrategia para la creación de las primeras reglas, poner las más genéricas y permisivas al final de la lista de control y las más específicas y restrictivas al principio, aunque esta regla no es aplicable al 100% de los casos.

Por último, el parámetro de control establece qué hacer una vez leída la lista de acceso.

* ‘stop’, por defecto, hace que cuando se lea la lista, ya no se lean más listas.
* ‘continue’ sigue leyendo los ‘who’ de la línea a pesar de haber encontrado coincidencia.
* ‘break’ sigue leyendo las demás listas de acceso.

{3}to dn.children="ou=usuaris,dc=exo,dc=cat"  by users write by \* none

Amb permís d’escriptura

{0}to attrs=userPassword by self write by anonymous auth by \* none

### **Acl’s per defecte**

*{0}to attrs=userPassword by self write by anonymous auth by \* none*

*{1}to attrs=shadowLastChange by self write by \* read*

*{2}to by read*

### **Configuració**

Es carregaran en En entrada {1} mdb:

*# admin user*

*{0} to by dn="cn=admin,dc=exo,dc=cat" manage by break*

*# ldap admin users*

*{1} to by set.expand="(([cn=ldapadmin,ou=serveis,dc=exo,dc=cat])/member)/entryDN & user" write by break*

*{2} to by set.expand="(([cn=ldapread,ou=serveis,dc=exo,dc=cat])/member)/entryDN & user" read by break*

La resta de permisos per defecte es deixen a sota

Test

per afegir registres:

ldapadd -x -w pass -D 'uid=ldapdolibarr,ou=usuaris,dc=exo,dc=cat' -H ldap://localhost -f file

# 

# Password Policy

<https://www.arctiq.ca/our-blog/2018/9/4/implementing-a-password-policy-in-an-ldap-directory/>

<https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/ldap-server/README-orig.md#olcpasswordhash-ssha512>

<https://www.zytrax.com/books/ldap/ch6/>

Primer de tot, carreguem el mòdul de password police ja que no està carregat per defecte

ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif

load the password policy module into the directory

ldapadd -Y EXTERNAL -H ldapi:///

*dn: cn=module{0},cn=config*

*changetype: modify*

*add: olcModuleLoad*

*olcModuleLoad: ppolicy*

Configure the overlay:

ldapadd -Y EXTERNAL -H ldapi:///

*dn: olcOverlay=ppolicy,olcDatabase={1}mdb,cn=config*

*objectClass: olcConfig*

*objectClass: olcOverlayConfig*

*objectClass: olcPPolicyConfig*

*olcOverlay: ppolicy*

*# the dn can be anywhere in the tree, we have chosen this*

*olcPPolicyDefault: cn=pwdpolicy,dc=exo,dc=cat*

*olcPPolicyHashCleartext: TRUE*

*olcPPolicyUseLockout: FALSE*

*olcPPolicyForwardUpdates: FALSE*

Definim la política de password:

Tenir en compte que s’ha de guardar al cn que hem indicar

ldapadd -D "cn=admin,dc=exo,dc=cat" -W -H ldap://localhost

*dn: cn=pwdpolicy,dc=exo,dc=cat*

*objectClass: pwdPolicy*

*objectClass: applicationProcess*

*objectClass: top*

*cn: pwdpolicy*

*pwdMaxAge: 0*

*pwdExpireWarning: 604800*

*pwdAttribute: userPassword*

*pwdInHistory: 0*

*pwdCheckQuality: 1*

*pwdMaxFailure: 0*

*pwdLockout: FALSE*

*pwdLockoutDuration: 300*

*pwdGraceAuthNLimit: 0*

*pwdFailureCountInterval: 0*

*pwdMustChange: TRUE*

*pwdMinLength: 8*

*pwdAllowUserChange: TRUE*

*pwdSafeModify: FALSE*

Hash sha256

Al igual que el password polices, haurem de carregar el mòdul:

ldapadd -Y EXTERNAL -H ldapi:///

*dn: cn=module{0},cn=config*

changetype: modify

add: olcModuleLoad

olcModuleLoad: pw-sha2.la

Important, usar {0} si no es crearà el module{1}

Definim el us del sha256

<https://www.zytrax.com/books/ldap/ch6/#password-hash>

ldapadd -Y EXTERNAL -H ldapi:///

*dn: olcDatabase={-1}frontend,cn=config*

*changetype: modify*

*replace: olcPasswordHash*

*olcPasswordHash: {SSHA256}*

# Altres documents

- [ldap-certs.md](./ldaps-certs.md)
- [passwd-policy.md](./passwd-policy.md)

