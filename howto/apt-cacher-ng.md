This documentation is still incomplete

apt-cacher-ng lets you speed up general install and upgrades and save internet bandwidth caching content from apt repositories

## client config

in the target VM we want to cache its repositories we add a file such as `/etc/apt/apt.conf.d/proxy`

and with the target IP of our cache server (your_hostname could be a domain or IP address)

```
Acquire::http { Proxy "http://your_hostname:3142"; }
```

## cache TLS repos

example with jitsi, where https connection is mandatory

let's consider we have the following content in our repo file `/etc/apt/sources.list.d/jitsi-stable.list`. Note that it is http protocol:

```
deb http://download.jitsi.org stable/
```

then, in the apt-cacher-ng I add a file such as `/etc/apt-cacher-ng/backends_jitsi`

with content

```
deb https://download.jitsi.org
```

and in `/etc/apt-cacher-ng/acng.conf`

I add the following line:

``
Remap-jitsi: download.jitsi.org ;  file:backends_jitsi
```

reference: https://blog.packagecloud.io/eng/2015/05/05/using-apt-cacher-ng-with-ssl-tls/
