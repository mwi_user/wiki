#!/bin/sh -e

gfs_disk_details() {
  gfid="${1}"
  if [ -z "${gfid}" ]; then
    echo "ERROR: no argument gfid found"
    return
  fi
  gfid_path="$(find /hddpool/*/*/.glusterfs -name "${gfid}*")"
  if [ -z "${gfid_path}" ]; then
    echo "NOT FOUND: this gfid is not present in that host"
    return
  fi
  inode="$( ls -i ${gfid_path} | cut -d' ' -f1)"
  vm_disk="$(find /hddpool/ -inum ${inode} | grep images | grep -oE '/vmhddstore.*')"
  echo "    ## du size "
  du -sh "${vm_disk}"
  echo "    ## du apparent size"
  du -sh --apparent-size "${vm_disk}"
  echo "    ## check image"
  qemu-img check "${vm_disk}" || true
}

gfids="$(grep -v '#' <<EOF
396c57c5-6114-4365-867c-6a4912562589
60e61d22-8564-4e0a-bdb4-d13c220ea251
af524a3f-23b5-4c1a-a5c4-845d34c270a9
bd51440a-b0e8-4d26-b84e-68cb7ce05bcb
EOF
)"

line="==============================================================="
for gfid in $(echo ${gfids}); do
  echo "${line}"
  echo "  # processing gfid ${gfid}"
  gfs_disk_details ${gfid}
done
echo "${line}"
