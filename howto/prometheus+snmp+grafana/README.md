# Prometheus + SNMP + Grafana
Un dels problemes que tenim com a eXO és la manca de monitorització de l'estat dels circuits dels nostres socis (ja sigui via FTTH o Ràdio). Una de les propostes per millorar aquesta situació és la d'utilitzar Prometheus com a base de dades time-series i Grafana per tal de gestionar la visualització de dades.

## Prometheus
### Instal·lació
Prometheus és un software per a la monitorització de tot tipus de sistemes. Es basa en una base de dades time-series (que indexa per moment temporal) i un conjunt d'eines que permeten recopilar informació i processar-la. Està basat en el llenguatge de programació Go.

En un entorn com el de la eXO ens interessa una solució que es pugui instal·lar sobre una màquina virtual. Les instruccions següents són vàlides per un sistema basat en Debian Buster.

Primer de tot ens hem de descàrregar la versió més recent de prometheus per a Linux x64:

`wget https://github.com/prometheus/prometheus/releases/download/v2.14.0/prometheus-2.14.0.linux-amd64.tar.gz`

Ara ho descomprimim i copiem els binaris a `/usr/bin`:

```
tar xvf prometheus-2.14.0.linux-amd64.tar.gz
cd prometheus-2.14.0.linux-amd64
cp prometheus /usr/local/bin/
cp promtool /usr/local/bin/

```


Després hem de preparar l'usuari de `prometheus` i el directori de configuracions:

```
# Creem el directori
mkdir /etc/prometheus
# Creem el directory
mkdir /var/lib/prometheus
# Creem l'usuari
useradd --no-create-home --shell /bin/false prometheus
# Canviem l'owner /etc/prometheus
chown -R prometheus:prometheus /etc/prometheus/
# Canviem l'owner /var/lib/prometheus
```

Finalment creem el unit de systemd a `/etc/systemd/system/prometheus.service`:

```
[Unit]
Description=Prometheus
Wants=network-online.target
After=network-online.target
 
[Service]
User=prometheus
Group=prometheus
Type=simple
ExecStart=/usr/local/bin/prometheus \
    --config.file /etc/prometheus/prometheus.yml \
    --storage.tsdb.path /var/lib/prometheus/ \
    --web.console.templates=/etc/prometheus/consoles \
    --web.console.libraries=/etc/prometheus/console_libraries
 
[Install]
WantedBy=multi-user.target
```


### SNMP Exporter

### Node Exporter
Aquest és el col·lector **per defecte** de Prometheus. Es tracta d'un col·lector sobre informació de sistemes Unix i Unix-like. Depenent del sistema operatius estaran disponibles unes opcions o unes altres. El seu funcionament és molt senzill, només necessita instalar-se el binari corresponent a la versió del nostre SO i programar un inici automàtic del dimoni.

Instal·lació sobre entorn Debian buster:

```
wget https://github.com/prometheus/node_exporter/releases/download/v0.16.0/node_exporter-0.16.0.linux-amd64.tar.gz
tar xvf node_exporter-0.16.0.linux-amd64.tar.gz
useradd --no-create-home --shell /bin/false node_exporter
cp node_exporter-0.16.0.linux-amd64/node_exporter /usr/local/bin
chown node_exporter:node_exporter /usr/local/bin/node_exporter
rm -rf node_exporter-0.16.0.linux-amd64.tar.gz node_exporter-0.16.0.linux-amd64
vim /etc/systemd/system/node_exporter.service

```

El contingut del unit de systemd és el següent:

```
[Unit]
Description=Node Exporter
Wants=network-online.target
After=network-online.target

[Service]
User=node_exporter
Group=node_exporter
Type=simple
ExecStart=/usr/local/bin/node_exporter --web.listen-address="<ipv4>:<port>"

[Install]
WantedBy=multi-user.target

```

Llavors haurem de fer scrapping sobre aquesta `<ipv4>:<port>`

### Links d'interés
- MIB Generator: https://stackoverflow.com/questions/56009729/prometheus-help-editing-configuring-snmp-exporters-generator-yml-file-for-cisc

