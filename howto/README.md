**Articles destacats**:

- [Integració BMX6 i BGP amb LEDE](bmx6-bgp-lede.md)
- [Servei d'accés a Internet per L2TP Aggregation Architecture (LAA)](arquitectura-laa-acces-inet)

# articles about building openwrt on devices

- https://gitlab.com/guifi-exo/wiki/tree/master/howto/install_openwrt_on_Sxt5_from_sources
- https://gitlab.com/guifi-exo/wiki/tree/master/howto/install_openwrt_in_Alfa_N5
- https://gitlab.com/guifi-exo/wiki/blob/master/howto/mikrotik-lede.md
- https://gitlab.com/guifi-exo/wiki/blob/master/howto/lime-sdk.md

# TODO backup / other refs

- http://mataro.guifi.net/wiki/index.php?n=Proxmox.DebianJessieNetinstall
- http://hacklabvalls.org/media/nou_node.pdf
- http://hacklabvalls.org/media/Manual-connexio-guifinet.pdf
- http://guifiamunt.guifi.net/wp-content/uploads/2013/12/CREAR-UN-USUARI-DE-GUIFINET-I-CREAR-UN-NODE.pdf
- Supernodes híbrids: https://nextcloud.datalab.es/index.php/s/WioZblkzru9NCPc
