_slapd_add_general_group() {

  # permission error -> src https://superuser.com/questions/1106609/no-write-access-to-parent/1106610#1106610

  cprint "add general group $1" 'altstyle'

  ldapadd -w "$PASSWD" -D "cn=admin,$ORG_DN" -H ldapi:/// <<EOF
dn: ou=$1,$ORG_DN
objectClass: top
objectClass: organizationalUnit
ou: $1
EOF

}

_slapd_add_user() {

if [[ -z $3 ]]; then
  mail=$1@example.com
else
  mail=$3
fi

  cprint "add user $1" 'altstyle'

  ldapadd -w "$PASSWD" -D "cn=admin,$ORG_DN" -H ldapi:/// <<EOF
dn: uid=$1,ou=users,$ORG_DN
objectClass: inetOrgPerson
uid: $1
displayName: $1
cn: $1
sn: $1
mail: $mail
userPassword: $2
EOF

}

_slapd_add_group() {

  cprint "add group $1" 'altstyle'

  ldapadd -w "$PASSWD" -D "cn=admin,$ORG_DN" -H ldapi:/// <<EOF
dn: cn=$1,ou=groups,$ORG_DN
objectClass: top
objectClass: groupOfNames
cn: $1
member: cn=admin,$ORG_DN
EOF

}

_slapd_add_user_to_group() {

  cprint "add user $1 to group $2" 'altstyle'

  ldapadd -w "$PASSWD" -D "cn=admin,$ORG_DN" -H ldapi:/// <<EOF
dn: cn=$2,ou=groups,$ORG_DN
changetype: modify
add: member
member: uid=$1,ou=users,$ORG_DN

EOF

}
