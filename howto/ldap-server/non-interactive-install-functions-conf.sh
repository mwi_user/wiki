_slapd_conf_init_install() {

  # clean backup to help slapd reconfiguration succeed (looks like a bug from the debian side)
  mkdir -p "/var/backup/weird-backups-slapd-$TS"
  mv -v /var/backups/slapd* "/var/backup/weird-backups-slapd-$TS" || true
  mv -v /var/backups/unknown* "/var/backup/weird-backups-slapd-$TS" || true

  # non-interactive variables for slapd reconfiguration
  # src https://unix.stackexchange.com/questions/362547/automating-slapd-install
  #   see other variables with: debconf-show slapd

  # get the values:
  #   debconf-get-selections | grep slapd
  #   cat /var/cache/debconf/passwords.dat | grep slapd

  # check variables https://stackoverflow.com/questions/29278743/how-to-check-if-multiple-variables-are-defined-or-not-in-bash/29280547#29280547
  if [ -z "$PASSWD" ] || [ -z "$ORG_DN" ] || [ -z "$ORG_DNS" ]; then

    cprint "  cannot continue. undefined variables PASSWD, ORG_DN or ORG_DNS. Check in $ENV_FILE"
    exit

  fi

  cat > debconf-slapd.conf <<EOF
slapd slapd/internal/generated_adminpw password $PASSWD
slapd slapd/internal/adminpw password $PASSWD
slapd slapd/password1 password $PASSWD
slapd slapd/password2 password $PASSWD
slapd shared/organization string $ORG_DN
slapd slapd/domain string $ORG_DNS
slapd slapd/backend select MDB
slapd slapd/move_old_database boolean true
slapd slapd/invalid_config boolean true
slapd slapd/dump_database select when needed
slapd slapd/purge_database boolean true
slapd slapd/no_configuration boolean false
slapd slapd/dump_database_destdir string /var/backups/slapd-VERSION
slapd slapd/ppolicy_schema_needs_update select abort installation
EOF

  export DEBIAN_FRONTEND=noninteractive
  cat debconf-slapd.conf | debconf-set-selections

  # does non-interactive reconfiguration
  dpkg-reconfigure slapd

  # remove temp file
  rm debconf-slapd.conf

}

_slapd_conf_set_pw() {

# thanks:
#   - https://serverfault.com/questions/556629/unknown-ldap-cn-config-admin-password
#   - https://unix.stackexchange.com/questions/362547/automating-slapd-install/363087#363087

# avoid config password to be in cleared text
# used some lines found in mkpw program -> src https://github.com/rda0/mkpw

if [ "$(command -v mkpasswd)" != "/usr/bin/mkpasswd" ]; then
  cprint "error: mkpasswd is not installed. To install run:\n    apt install whois"
  exit 1
fi
rounds=10000
salt_charset='./a-zA-Z0-9'
salt_len=16
salt=$(head -c"${salt_len}" < <(LC_CTYPE=C tr -dc "${salt_charset}" < /dev/urandom))
shapasswd="$(echo -n "${PASSWD}" | /usr/bin/mkpasswd -s -m "sha-512" -R "${rounds}" -S "${salt}")"
config_pwd="{CRYPT}$shapasswd"

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
dn: olcDatabase={0}config,cn=config
changetype: modify
replace: olcRootPW
olcRootPW: $config_pwd
EOF

}

_slapd_conf_enable_logs() {

# thanks https://www.cyrill-gremaud.ch/howto-install-openldap-2-4-server/

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
changetype: modify
replace: olcLogLevel
olcLogLevel: stats
EOF
}

_slapd_conf_unique_uid_mail_attrs() {

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
# import the unique module library
dn: cn=module{1},cn=config
cn: module{1}
objectClass: olcModuleList
olcModuleLoad: unique
olcModulePath: /usr/lib/ldap

# define the module overlay
dn: olcOverlay={0}unique,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcUniqueConfig
olcOverlay: {0}unique
olcUniqueBase: $ORG_DN
olcUniqueAttribute: uid
olcUniqueAttribute: mail
EOF

}

_slapd_conf_group_membership_integrity_consistency() {

  cprint 'group membership, integrity and consistency 1/3'

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
# import the memberof module library
dn: cn=module{2},cn=config
cn: module{2}
objectClass: olcModuleList
olcModuleLoad: memberof
olcModulePath: /usr/lib/ldap

# configure the overlay
dn: olcOverlay={1}memberof,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcMemberOf
objectClass: olcOverlayConfig
objectClass: top
olcOverlay: {1}memberof
olcMemberOfDangling: ignore
olcMemberOfRefInt: TRUE
olcMemberOfGroupOC: groupOfNames
olcMemberOfMemberAD: member
olcMemberOfMemberOfAD: memberOf
EOF

  cprint 'group membership, integrity and consistency 2/3'

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
# add the refint module to the existing cn=module{2},cn=config entry
dn: cn=module{2},cn=config
changetype: modify
add: olcModuleLoad
olcModuleLoad: refint
EOF

  cprint 'group membership, integrity and consistency 3/3'

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
# configure the refint overylay
dn: olcOverlay={2}refint,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcRefintConfig
objectClass: top
olcOverlay: {2}refint
olcRefintAttribute: memberof member manager owner
EOF

}

_slapd_conf_passwd_policy() {

  cprint 'password policy 1/4'

  ldapadd -Y EXTERNAL -H ldapi:/// -f /etc/ldap/schema/ppolicy.ldif

  cprint 'password policy 2/4'

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=module{3},cn=config
objectClass: olcModuleList
cn: module{3}
olcModuleLoad: ppolicy.la
EOF

  cprint 'password policy 3/4'

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
dn: olcOverlay=ppolicy,olcDatabase={1}mdb,cn=config
objectClass: olcConfig
objectClass: olcOverlayConfig
objectClass: olcPPolicyConfig
olcOverlay: ppolicy
# the dn can be anywhere in the tree, we have chosen this
olcPPolicyDefault: cn=defaultpwdpolicy,$ORG_DN
olcPPolicyHashCleartext: TRUE
olcPPolicyUseLockout: FALSE
olcPPolicyForwardUpdates: FALSE
EOF

  cprint 'password policy 4/4'

  ldapadd -w "$PASSWD" -D "cn=admin,$ORG_DN" -H ldapi:/// <<EOF
# add default policy to DIT
# attributes preceded with # indicate the defaults and
# can be omitted
# passwords must be reset every 1 year (pwdMaxAge 31536000)
# have a minimum length of 6 (pwdMinLength: 6), and users will
# get a expiry warning starting 1 week (pwdExpireWarning: 604800) before
# expiry, when the consecutive fail attempts exceed 5 (pwdMaxFailure: 5)
# the count will be locked for 5 minutes (pwdLockoutDuration: 300) before
# the user can login again, users do not need to supply the old
# password when changing (pwdSafeModify: FALSE)
# Users can change their own password (pwdAllowUserChange: TRUE)

dn: cn=defaultpwdpolicy,$ORG_DN
objectClass: pwdPolicy
objectClass: applicationProcess
objectClass: top
cn: defaultpwdpolicy
pwdMaxAge: 0
pwdExpireWarning: 604800
pwdAttribute: userPassword
pwdInHistory: 0
pwdCheckQuality: 1
pwdMaxFailure: 0
pwdLockout: FALSE
pwdLockoutDuration: 300
pwdGraceAuthNLimit: 0
pwdFailureCountInterval: 0
pwdMustChange: TRUE
pwdMinLength: 8
pwdAllowUserChange: TRUE
pwdSafeModify: FALSE
EOF

}

_slapd_conf_passwd_hashing() {

  cprint 'password hash 1/2'

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=module,cn=config
objectClass: olcModuleList
cn: module
olcModuleLoad: pw-sha2.la
EOF

  cprint 'password hash 2/2'

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
dn: olcDatabase={-1}frontend,cn=config
changetype: modify
replace: olcPasswordHash
olcPasswordHash: {SSHA512}
EOF

}

_slapd_conf_prepare_tls_keys() {

  tls_path="/etc/ldap/tls"
  mkdir -p "$tls_path"
  cp -rpLv "$LE_PATH/$LE_CACERT"  "$tls_path/$LE_CACERT"
  cp -rpLv "$LE_PATH/$LE_CERT"    "$tls_path/$LE_CERT"
  cp -rpLv "$LE_PATH/$LE_PRIVKEY" "$tls_path/$LE_PRIVKEY"
  #chown -R openldap.openldap /etc/ldap/tls

}

_slapd_conf_tls() {

  # requires to have the tls keys configured
  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
dn: cn=config
changetype: modify
add: olcTLSCipherSuite
#olcTLSCipherSuite: SECURE
olcTLSCipherSuite: NORMAL
-
add: olcTLSCRLCheck
olcTLSCRLCheck: none
-
add: olcTLSVerifyClient
olcTLSVerifyClient: never
-
add: olcTLSCACertificateFile
olcTLSCACertificateFile: $tls_path/$LE_CACERT
-
add: olcTLSCertificateFile
olcTLSCertificateFile: $tls_path/$LE_CERT
-
add: olcTLSCertificateKeyFile
olcTLSCertificateKeyFile: $tls_path/$LE_PRIVKEY
-
add: olcTLSProtocolMin
olcTLSProtocolMin: 3.3
# -
# # looks like important https://books.google.es/books?id=ExZ4DwAAQBAJ&pg=PT189&lpg=PT189&dq=%22olcLocalSSF:+128%22&source=bl&ots=fMeFkAX7Ch&sig=ACfU3U2k_z_Au-CeLPlhRDcc36bm2Bdz3A&hl=es&sa=X&ved=2ahUKEwiAu9b2iv_nAhWlURUIHQZNBbYQ6AEwBHoECAoQAQ#v=onepage&q=%22olcLocalSSF%3A%20128%22&f=false
# add: olcSecurity
# olcSecurity: ssf=128
# -
# # but continue using ldapi (olcLocalSSF part) -> src https://www.openldap.org/lists/openldap-technical/201807/msg00032.html
# add: olcLocalSSF
# olcLocalSSF: 128
# -
# add: olcSecurity
# olcSecurity: tls=128
EOF

}

_slapd_conf_acl() {

  ldapadd -Y EXTERNAL -H ldapi:/// <<EOF
#cat <<EOF
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
olcAccess: {0}to attrs=userPassword by self write by anonymous auth by * none
olcAccess: {1}to attrs=shadowLastChange by self write by * read
olcAccess: {2}to * by * read
-
add: olcAccess
olcAccess: {0}to attrs=shadowLastChange by self write by * read
olcAccess: {1}to attrs=userPassword by self write by group.exact="cn=ldap-admins,ou=services,$ORG_DN" write by anonymous auth by * none
olcAccess: {2}to * by self write by group.exact="cn=ldap-admins,ou=services,$ORG_DN" write by users read
EOF

}
