# uppercase vars
source non-interactive-install-env
nc_path="/var/www/html/nextcloud"
nc_config="s01"

cat > config-nextcloud.sh <<EOF
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config hasMemberOfFilterSupport       1'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config lastJpegPhotoLookup            0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapAgentName                  cn=admin,$ORG_DN'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapAgentPassword              $PASSWD'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapBase                       $ORG_DN'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapBaseGroups                 $ORG_DN'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapBaseUsers                  $ORG_DN'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapCacheTTL                   600'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapConfigurationActive        1'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapEmailAttribute             mail'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapExperiencedAdmin           0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapGidNumber                  gidNumber'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapGroupDisplayName           cn'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapGroupFilter                "(&(|(objectclass=groupOfNames)))"'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapGroupFilterMode            0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapGroupFilterObjectclass     groupOfNames'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapGroupMemberAssocAttr       member'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapHost                       ldap://localhost'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapLoginFilter                "(&(&(|(objectclass=inetOrgPerson)))(|(uid=%uid)(|(mailPrimaryAddress=%uid)(mail=%uid))))"'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapLoginFilterEmail           1'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapLoginFilterMode            0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapLoginFilterUsername        1'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapNestedGroups               0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapPagingSize                 500'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapPort                       389'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapTLS                        0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapUserAvatarRule             default'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapUserDisplayName            cn'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapUserFilter                 "(&(|(objectclass=inetOrgPerson)))"'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapUserFilterMode             0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapUserFilterObjectclass      inetOrgPerson'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapUuidGroupAttribute         auto'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config ldapUuidUserAttribute          auto'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config turnOffCertCheck               0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config turnOnPasswordChange           0'
su www-data -s /bin/sh -c 'php $nc_path/occ ldap:set-config $nc_config useMemberOfToDetectMembership  1'
EOF

chmod +x config-nextcloud.sh
