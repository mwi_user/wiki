# Instal·lació

Considerem la instal·lació del servidor L2TP/PPPoE/IPoE accel-ppp en una distribució
Debian 9. Aquest servidor no compta de moment amb paqueteria oficial per Debian. Caldrà
compilar-lo.

## Preparatius i compilació

Per compilar correctament aquest paquet caldrà instal·lar els paquets:

`apt install git build-essential cmake libsnmp-dev linux-headers-amd64 libpcre3-dev libssl-dev liblua5.1-0-dev`

Ens situem al directory `/usr/local/src` i clonem el git del codi oficial. En el nostre
cas hem triat el tag corresponent a la darrera versió estable.

```
# git clone https://git.code.sf.net/p/accel-ppp/code accel-ppp
# cd accel-ppp
./accel-ppp# git checkout 1.12.0
```

Creem el directori `build` i executem `cmake` per tal d'afegir a la compilació
diferents opcions (IPoE, VLAN-mon, subagent SNMP, etc.). Aquestes opcions generen nous mòduls de kernel o bé afegir funcionalitats extra:

```
./accel-ppp/build# cmake -DKDIR=/usr/src/linux-headers-$(uname -r) -DCPACK_TYPE=Debian9 -DBUILD_IPOE_DRIVER=TRUE -DBUILD_VLAN_MON_DRIVER=TRUE -DRADIUS=TRUE -DNETSNMP=TRUE -DLUA=TRUE ..
./accel-ppp/build# make
```

**alerta** no compil·lar de forma tal com `make -j4`, ja que no funcionaria

Si no obtenim cap error en la compilació procedim a instal·lar els nous
mòduls de kernel que s'han compilat:

```
./accel-ppp/build# cp drivers/ipoe/driver/ipoe.ko /lib/modules/$(uname -r)
./accel-ppp/build# cp drivers/vlan_mon/driver/vlan_mon.ko /lib/modules/$(uname -r)
./accel-ppp/build# depmod -a
./accel-ppp/build# modprobe vlan_mon
./accel-ppp/build# modprobe ipoe
./accel-ppp/build# echo "vlan_mon" >> /etc/modules
./accel-ppp/build# echo "ipoe" >> /etc/modules
```

**Nota important: en cada actualització del kernel de debian s'hauran de tornar a compil·lar amb els passos d'aquesta secció.**

## Instal·lació del paquet

Ara ja podem crear el paquet amb la comanda `cpack` inclosa dins el paquet `cmake`:

```
./accel-ppp/build# cpack -G DEB
./accel-ppp/build# apt install ./accel-ppp.deb
```

Habilitem el servei en el sistema operatiu:

```
./accel-ppp/build# systemctl enable accel-ppp
```

## Configuració
Accel-PPP és un servei altament personalitzable però utilitza un sol arxiu de configuració `accel-ppp.conf`. Cal força feina per crear un arxiu de configuracions adequats a les nostres necessitats.
En el nostre cas hem usat aquest:

```
[modules]
log_file
#log_syslog
#log_tcp
#log_pgsql

#pptp
l2tp
pppoe

auth_mschap_v2
auth_mschap_v1
auth_chap_md5
#auth_pap

radius

ippool
ipv6_nd
ipv6_dhcp
ipv6pool

# if you need both static and dynamic addresses then place ippool below chap-secrets -> src https://accel-ppp.org/forum/viewtopic.php?t=99
#chap-secrets

#pppd_compat

#shaper
net-snmp
#logwtmp
#connlimit

#net-accel-dp

[core]
log-error=/var/log/accel-ppp/core.log
thread-count=4

[common]
#single-session=replace
#sid-case=upper
#sid-source=seq

[ppp]
verbose=1
min-mtu=1280
mtu=1492
mru=1492
#accomp=deny
#pcomp=deny
ccp=0
check-ip=1
#mppe=require
ipv4=require
#ipv6=deny
ipv6=allow
ipv6-intf-id=random
ipv6-peer-intf-id=calling-sid
ipv6-accept-peer-intf-id=1
lcp-echo-interval=10
#lcp-echo-failure=3
lcp-echo-timeout=30
unit-cache=1
#unit-preallocate=1
# Deny a second stablished connection from the same PPP subscription
single-session=deny

[auth]
#any-login=0
#noauth=0

[pptp]
verbose=1
#echo-interval=30

[pppoe]
verbose=1
ac-name=bng.exo.cat
#ifname=pppoe%d
#service-name=yyy
#pado-delay=0
#pado-delay=0,100:100,200:200,-1:500
#called-sid=mac
#tr101=1
#padi-limit=0
#ip-pool=pppoe
#sid-uppercase=0

# Dynamic VLAN ifaces allocation for Sarenet: Every CPE is mapped to diferent VLAN
vlan-mon=bstrm0,182,186,200,203,205

# Dynamic VLAN ifaces allocation for Emagina: All PPPoE session com from the same VLAN
vlan-mon=bstrm1,60

vlan-timeout=60
# Define VLAN iface name format. It must match with those dynamically created
vlan-name=%I.%N
#interface=eth1,padi-limit=1000,net=accel-dp

# Listening PPPoE VLAN ifaces created dynamically
interface=re:bstrm0\.
interface=re:bstrm1\.
interface=eth4

[l2tp]
verbose=1
#dictionary=/usr/local/share/accel-ppp/l2tp/dictionary
hello-interval=10
#timeout=60
#rtimeout=1
#rtimeout-cap=16
retransmit=3
#recv-window=16
host-name=bng.exo.cat
#ifname=l2tp%d
#dir300_quirk=0
#secret=
#dataseq=allow
#reorder-timeout=0
#ip-pool=l2tp
# We enable compatability to anti-RFC vendors
avp_permissive=1
# Overrride the MTU PPP settings for PPPoL2TP connections
ppp-max-mtu=1450

[dns]
dns1=1.1.1.1
dns2=8.8.8.8

[wins]
#wins1=172.16.0.1
#wins2=172.16.1.1

[client-ip-range]
disable
#0.0.0.0/0

[ip-pool]
gw-ip-address=192.168.155.1
#vendor=Cisco
attr=Framed-Pool
185.190.179.42-50,name=ppp-ipv4-dyn-a
192.168.156.2-254,name=ppp-ipv4-dyn-z

[log]
log-file=/var/log/accel-ppp/accel-ppp.log
log-emerg=/var/log/accel-ppp/emerg.log
log-fail-file=/var/log/accel-ppp/auth-fail.log
#log-debug=/dev/stdout
#syslog=accel-pppd,daemon
#log-tcp=127.0.0.1:3000
copy=0
#color=1
per-user-dir=/var/log/accel-ppp/users
#per-session-dir=per_session
#per-session=1
#default level
level=4

[log-pgsql]
conninfo=user=log
log-table=log

[pppd-compat]
#ip-pre-up=/etc/ppp/ip-pre-up
ip-up=/etc/ppp/ip-up
ip-down=/etc/ppp/ip-down
ip-change=/etc/ppp/ip-change
radattr-prefix=/var/run/radattr
verbose=1

[chap-secrets]
#gw-ip-address=192.168.155.1
#chap-secrets=/etc/ppp/chap-secrets
#encrypted=0
#username-hash=md5

[radius]
dictionary=/usr/local/share/accel-ppp/radius/dictionary
nas-identifier=accel-ppp
#nas-ip-address=127.0.0.1
gw-ip-address=192.168.155.1
server=127.0.0.1,<radius-password-here>,auth-port=18120,acct-port=0,req-limit=50,fail-timeout=0,max-fail=10,weight=1
dae-server=127.0.0.1:3799,<nas-dae-server-password-here>
verbose=2

[shaper]
#attr=Filter-Id
#down-burst-factor=0.1
#up-burst-factor=1.0
#latency=50
#mpu=0
#mtu=0
#r2q=10
#quantum=1500
#moderate-quantum=1
#cburst=1534
#ifb=ifb0
up-limiter=police
down-limiter=tbf
#leaf-qdisc=sfq perturb 10
#leaf-qdisc=fq_codel [limit PACKETS] [flows NUMBER] [target TIME] [interval TIME] [quantum BYTES] [[no]ecn]
#rate-multiplier=1
#fwmark=1
verbose=1

[cli]
verbose=1
telnet=127.0.0.1:2000
tcp=127.0.0.1:2001
#password=123

[snmp]
master=0
agent-name=accel-ppp

[connlimit]
limit=10/min
burst=3
timeout=60

[ipv6-pool]
########
# IPV6 pool of PPP interfaces
# This addresses only apply under network local interfaces
2a00:1508:0100:e000::/54,64
########
# IPv6 pool of delegated prefixes
#delegate=2a00:1508:100:8000::/50,60
#fc00:0:1::/48,64
#delegate=fc00:1::/36,48
# If you use RADIUS just need to configure Delegated-IPv6-Prefix as a reply attribute

[ipv6-dns]
#fc00:1::1
#fc00:1::2
#fc00:1::3
#dnssl=suffix1.local.net
#dnssl=suffix2.local.net.

[ipv6-dhcp]
verbose=1
pref-lifetime=604800
valid-lifetime=2592000
route-via-gw=1

[accel-dp]
socket=/var/run/accel-dp.sock

```
Es basa en activar el servei de PPPoE i L2TP. En el nostre cas no fem servir IPoE ni d'altres protocols. Fem servir la creació dinàmica de interfícies VLAN (`vlan-mon=bstrm0,182,186,200,203,205`) pel cas de serveis portadors tipus _bitstream_.

Per fer un seguiment individualitzat dels missatges de log per usuari, activem l'opció `per-user-dir=/var/log/accel-ppp/users`. D'altra banda, basem tot el procés d'AAA en un servidor RADIUS. Aquesta part està configurada a la secció `[radius]`. Accel-PPP permet activar un servidor de control de la connexió (`dae-server=127.0.0.1:3799,<nas-dae-server-password-here>`). Aquest servidor permet a RADIUS d'enviar ordres de connexió i desconnexió administratives.
Situem l'arxiu `accel-ppp.conf` a `/etc/accel-ppp.conf` i arrenquem el servei:

```
service accel-ppp start
```

Podem opcionalment configurar la rotació dels arxius de log a `/etc/logrotate.d/accel-ppp`:

```
/var/log/accel-ppp/*.log {
        compress
        rotate 5
        size 300k
        missingok
        notifempty
        sharedscripts
        postrotate
                test -r /var/run/accel-pppd.pid && kill -HUP `cat /var/run/accel-pppd.pid`
        endscript
}
```

# Provar configuracions

Amb canvis de configuració poden aparèixer errors que facin que accel-ppp ni funcioni ni ho notifiqui als logs (per exemple `ippool: cann't parse '192.168.156.1,name=ppp-ipv4-dyn-z'`), així doncs provar d'arrencar per rebre els missatges d'error des de la terminal on s'està:

    /usr/sbin/accel-pppd -c /etc/accel-ppp.conf

De moment no coneixem la comanda que permeti validar configuracions així que es recomana provar-ho en un entorn experimental

És possible aplicar noves configuracions sense reiniciar el daemon. En aquest cas podem executar:

```
root@bng:~# accel-ppp reload
```

Si apareixen errors al log `/var/log/accel-ppp/accel-ppp.log` els canvis no seran aplicats.

# Veure estat de connexions

La comanda bàsica per veure dades detalladades de les connexions és `accel-cmd`:

```
root@bng:~# accel-cmd show sessions "ifname,sid,username,calling-sid,ip,ip6,ip6-dp,type,uptime"
  ifname   |        sid       |      username       |    calling-sid    |       ip       |                    ip6                    |         ip6-dp          | type  |  uptime
-----------+------------------+---------------------+-------------------+----------------+-------------------------------------------+-------------------------+-------+----------
 ppp-34876 | d24fa4ffe4aaaa2e | user1@exo.cat       | ca:2a:fb:59:1e:97 | 185.190.179.12 | 2a00:1508:100:e001::b/64                  |                         | pppoe | 00:43:43
 ppp-98762 | d24fa4ffe4aaaa2f | test                | 6c:3b:6b:88:44:24 | 185.190.179.10 |                                           |                         | pppoe | 00:43:43
 ppp-54542 | d24fa4ffe4aaaa30 | 54542@exo.cat       | 10.228.193.126    | 185.190.179.21 | 2a00:1508:100:e000:dc99:b80c:25ef:5053/64 | 2a00:1508:100:8000::/60 | l2tp  | 00:43:41
 ppp-27172 | d24fa4ffe4aaaa31 | user2@exo.cat       | 10.1.8.200        | 185.190.179.9  | 2a00:1508:100:e002:cdf0:9398:3ffb:5f57/64 | 2a00:1508:100:8010::/60 | l2tp  | 00:43:41
```

podem tenir una visió global del servidor amb `accel-cmd show stat`, exemple real:

```
uptime: 3.05:02:03
cpu: 0%
mem(rss/virt): 8132/442296 kB
core:
  mempool_allocated: 689446
  mempool_available: 488802
  thread_count: 4
  thread_active: 1
  context_count: 153
  context_sleeping: 0
  context_pending: 0
  md_handler_count: 305
  md_handler_pending: 0
  timer_count: 178
  timer_pending: 0
sessions:
  starting: 0
  active: 76
  finishing: 0
l2tp:
  tunnels:
    starting: 0
    active: 56
    finishing: 0
  sessions (control channels):
    starting: 0
    active: 56
    finishing: 0
  sessions (data channels):
    starting: 0
    active: 56
    finishing: 0
pppoe:
  starting: 0
  active: 20
  delayed PADO: 0
  recv PADI: 40
  drop PADI: 0
  sent PADO: 40
  recv PADR(dup): 38(0)
  sent PADS: 38
  filtered: 0
radius(1, 192.168.95.45):
  state: active
  fail count: 0
  request count: 0
  queue length: 0
  auth sent: 1282
  auth lost(total/5m/1m): 0/0/0
  auth avg query time(5m/1m): 2/0 ms
```

# Logs

Es recomana tenir espai pels logs, en cas de no poder tenir espai, moure-ho a un altre directori o disc, i també és important aplicar logrotate perquè poden arribar a pesar bastant

`/etc/logrotate.d/accelppp`

```
/var/log/accel-ppp/*.log {
  compress
  rotate 5
  size 10M
  missingok
  notifempty
  sharedscripts
  postrotate
    test -r /var/run/accel-pppd.pid && kill -HUP `cat /var/run/accel-pppd.pid`
  endscript
}

/var/log/accel-ppp/users/*.log {
  compress
  rotate 5
  size 10M
  missingok
  notifempty
  sharedscripts
  postrotate
    test -r /var/run/accel-pppd.pid && kill -HUP `cat /var/run/accel-pppd.pid`
  endscript
}
```

`kill -HUP` és la forma que té accelppp segons la doc oficial [per obligar a l'aplicació a tancar el fitxer per tal de poder moure'l i comprimir-lo](https://unix.stackexchange.com/questions/440004/why-is-kill-hup-used-in-logrotate-in-rhel-is-it-necessary-in-all-cases)

documentació oficial de logrotate https://accel-ppp.org/wiki/doku.php?id=faq

# Utilitats eXO per accel-ppp

https://gitlab.com/pedrolab/accel-ppp-utils

# Referències

referències utilitzades per fer aquesta documentació i altres referències d'interès

- https://accel-ppp.org/wiki/doku.php?id=compilation_debian
- https://serverfault.com/questions/574121/is-it-possible-for-l2tp-vpn-to-do-auto-route-configuration-for-client-during-con
- https://accel-ppp.org/wiki/doku.php?id=configfile#ipv6-pool
- https://accel-ppp.org/wiki/doku.php?id=configsoho
